package models

import (
	"github.com/jinzhu/gorm"
)

type Topic struct {
	gorm.Model
	NameTopic  string         `json:"name_topic,omitempty" gorm:"size:300;unique;not null"`
	Challenges ListChallenges `json:"challenges_of_topic,omitempty" gorm:"foreignkey:IDTopic;association_foreignkey:ID"`
}

type ListTopics []Topic

func (this *ListTopics) CountTopic() int {
	db := OpenDB()
	defer db.Close()

	return int(db.Find(&this).RowsAffected)
}

func (this *ListTopics) GetAllTopicNonChallenge() error {
	db := OpenDB()
	defer db.Close()

	return db.Find(&this).Error
}

func (this *ListTopics) GetAllTopic() error {
	db := OpenDB()
	defer db.Close()

	err := db.Find(&this).Error
	if err != nil {
		return err
	}

	for i, value := range *this {
		challenges, err := GetChallengesOfTopic(value)
		if err != nil {
			return err
		}
		(*this)[i].Challenges = challenges
	}
	return nil
}

func (this *ListTopics) GetTopicLimit(begin, limit int) error {
	db := OpenDB()
	defer db.Close()

	err := db.Offset(begin).Limit(limit).Find(&this).Error
	if err != nil {
		return err
	}

	for i, value := range *this {
		challenges, err := GetChallengesOfTopic(value)
		if err != nil {
			return err
		}
		(*this)[i].Challenges = challenges
	}
	return nil
}

func (this *Topic) GetTopic() error {
	db := OpenDB()
	defer db.Close()

	err := db.Find(&this).Error
	if err != nil {
		return err
	}

	challenges, err := GetChallengesOfTopic(*this)
	if err != nil {
		return err
	}

	this.Challenges = challenges
	return nil
}

func (this *Topic) GetTopicWithLimitChallenge(begin, limit int) error {
	db := OpenDB()
	defer db.Close()

	err := db.Find(&this).Error
	if err != nil {
		return err
	}

	return this.Challenges.GetChallengeOfTopicLimit(*this, begin, limit)
}

func (this *Topic) CreateTopic() error {
	db := OpenDB()
	defer db.Close()

	return db.Create(&this).Error
}

func (this *Topic) Update() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&Topic{}).Where("id = ?", this.ID).Update(&this).Error
}

func (this *Topic) Delete() error {
	db := OpenDB()
	defer db.Close()

	return db.Unscoped().Where("id = ?", this.ID).Delete(&this).Error
}

func (this *Topic) CheckExist() bool {
	db := OpenDB()
	defer db.Close()

	tmp := Topic{}
	return db.Where("id = ? or name_topic = ?", this.ID, this.NameTopic).Find(&tmp).RowsAffected != 0
}

func (this *Topic) GetTopicOfChallenge(challenge Challenge) error {
	db := OpenDB()
	defer db.Close()

	return db.Where("id = ?", challenge.IDTopic).First(&this).Error
}
