package models

import "github.com/jinzhu/gorm"

type Document struct {
	gorm.Model
	Name      string `json:"name,omitempty" gorm:"type:TEXT"`
	Link      string `json:"link,omitempty" gorm:"type:TEXT"`
	LocalFile string `json:"local_file" gorm:"type:TEXT"`
	IDLesson  uint   `json:"id_lesson, omitempty" gorm:"foreignkey:lessons"`
}

func (this *Document) Create() {
	db := OpenDB()
	defer db.Close()

	db.Create(&this)
}

func GetAllDocByLessonID(idLesson uint) []Document {
	db := OpenDB()
	defer db.Close()

	var docs []Document
	db.Model(&docs).Where("id_lesson = ?", idLesson).Find(&docs)
	return docs
}
