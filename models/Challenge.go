package models

import (
	"github.com/jinzhu/gorm"
)

type Challenge struct {
	gorm.Model
	Name     string    `json:"name_challenge,omitempty" gorm:"size:300;not null"`
	IDTopic  uint      `json:"id_topic,omitempty" gorm:"foreignkey:topic"`
	Problems []Problem `json:"problems_of_challenge,omitempty" gorm:"many2many:threads;"`
	Groups   []Group   `json:"exercises_in_group,omitempty" gorm:"many2many:exercises_in_group"`

	/** TODO: 1 là easy -> 4  */
	Level int `json:"level,omitempty" gorm:"not null;default:'1'"`
}

type ListChallenges []Challenge

func (this *Challenge) GetChallenge() error {
	db := OpenDB()
	defer db.Close()

	return db.First(&this).Error
}

func (this *Group) GetChallenge(begin, limit int) error {
	db := OpenDB()
	defer db.Close()

	db.Find(&this)

	return db.Model(&this).Offset(begin).Limit(limit).Association("Challenges").Find(&this.Challenges).Error
}

func CountChallengesOfTopic(topic Topic) int {
	db := OpenDB()
	defer db.Close()

	temp := ListChallenges{}
	return int(db.Where("id_topic = ?", topic.ID).Find(&temp).RowsAffected)
}

func CountChallengesOfGroup(group Group) int {
	db := OpenDB()
	defer db.Close()

	temp := ListChallenges{}
	db.Model(&group).Association("Challenges").Find(&temp)

	return len(temp)
}

func GetChallengesOfTopic(newTopic Topic) (ListChallenges, error) {
	db := OpenDB()
	defer db.Close()

	listChallenges := ListChallenges{}
	err := db.Where("id_topic = ?", newTopic.ID).Find(&listChallenges).Error
	if err != nil {
		return nil, err
	}

	return listChallenges, err
}

func (this *ListChallenges) GetChallengeByTopicID(id int) error {
	db := OpenDB()
	defer db.Close()

	return db.Where("id_topic = ?", id).Find(&this).Error
}

func (this *Challenge) CreateChallenge() error {
	db := OpenDB()
	defer db.Close()

	return db.Create(&this).Error
}

func (this *ListChallenges) GetChallengeOfTopicLimit(topic Topic, begin, limit int) error {
	db := OpenDB()
	defer db.Close()

	return db.Where("id_topic = ?", topic.ID).Offset(begin).Limit(limit).Find(&this).Error
}

func CountProblemOfChallenge(challenge Challenge) int {
	db := OpenDB()
	defer db.Close()

	temp := ListProblems{}
	db.Model(&challenge).Association("Problems").Find(&temp)
	return len(temp)
}

func (this *Challenge) GetProblemOfChallengeLimit(begin, limit int) error {
	db := OpenDB()
	defer db.Close()

	if err := db.Find(&this).Error; err != nil {
		return err
	}

	return db.Model(&this).Offset(begin).Limit(limit).Association("Problems").Find(&this.Problems).Error
}

func (this *Challenge) GetChallengeAccountPremium() error {
	db := OpenDB()
	defer db.Close()

	if err := db.First(&this, "id = ?", this.ID).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Problems").Find(&this.Problems).Error; err != nil {
		return err
	}

	for i, value := range this.Problems {
		inputOutput, err := GetInputOutputOfProblem(value)
		if err != nil {
			return err
		}
		this.Problems[i].InputsOutputsFormat = inputOutput
	}

	return nil
}

func (this *Challenge) GetChallengeByID() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&this).Where("id = ?", this.ID).First(&this).Error
}

func (this *Challenge) GetChallengeAccountNormal() error {
	db := OpenDB()
	defer db.Close()

	if err := db.First(&this, "id = ?", this.ID).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Where("premium = ?", 0).Association("Problems").Find(&this.Problems).Error; err != nil {
		return err
	}

	for i, value := range this.Problems {
		inputOutput, err := GetInputOutputOfProblem(value)
		if err != nil {
			return err
		}
		this.Problems[i].InputsOutputsFormat = inputOutput
	}

	return nil
}

func (this *ListChallenges) GetAllChallenge() error {
	db := OpenDB()
	defer db.Close()

	return db.Find(&this).Error
}

func (this *Challenge) UpdateChallenge() error {
	db := OpenDB()
	defer db.Close()

	if err := GetListProblem(this.Problems); err != nil {
		return err
	}

	if err := db.Model(&this).Updates(&this).Error; err != nil {
		return err
	}

	if err := db.Debug().Model(&this).Association("Problems").Clear().Error; err != nil {
		return nil
	}

	return db.Debug().Model(&this).Association("Problems").Append(&this.Problems).Error
}

func GetListChallenges(challenges []Challenge) error {
	db := OpenDB()
	defer db.Close()

	for i, val := range challenges {
		if err := val.GetChallenge(); err != nil {
			return err
		}
		challenges[i] = val
	}
	return nil
}

func (this *Challenge) CheckExist() bool {
	db := OpenDB()
	defer db.Close()

	temp := Challenge{}
	return db.Where("id = ? or name = ?", this.ID, this.Name).Find(&temp).RowsAffected != 0
}
