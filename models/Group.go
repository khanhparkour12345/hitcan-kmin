package models

import (
	"github.com/jinzhu/gorm"
)

type Group struct {
	gorm.Model
	GroupName  string      `json:"group_name,omitempty" gorm:"size:500;not null;unique"`
	Accounts   []Account   `json:"accounts_of_group,omitempty" gorm:"many2many:account_in_group;"`
	Challenges []Challenge `json:"exercises_in_group,omitempty" gorm:"many2many:exercises_in_group"`
}

type ListGroups []Group

func (this *Group) GetGroupDetail() error {
	db := OpenDB()
	defer db.Close()

	if err := db.Find(&this).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Challenges").Find(&this.Challenges).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Accounts").Find(&this.Accounts).Error; err != nil {
		return err
	}

	return nil
}

func (this *Group) GetGroup() error {
	db := OpenDB()
	defer db.Close()

	return db.Find(&this).Error
}

func (this *Group) CreateGroup(accounts []Account, challenges []Challenge) error {
	db := OpenDB()
	defer db.Close()

	if err := db.Create(&this).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Accounts").Append(&accounts).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Challenges").Append(&challenges).Error; err != nil {
		return err
	}

	for _, val := range accounts {
		val.Premium = 1
		if err := val.UpdatePremium(); err != nil {
			return err
		}
	}

	return nil
}

func (this *Group) UpdateGroup(accounts []Account, challenges []Challenge) error {
	db := OpenDB()
	defer db.Close()

	if err := db.Model(&this).Update(&this).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Accounts").Clear().Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Accounts").Append(&accounts).Error; err != nil {
		return err
	}

	for _, val := range accounts {
		val.Premium = 1
		if err := val.UpdatePremium(); err != nil {
			return err
		}
	}

	if err := db.Model(&this).Association("Challenges").Clear().Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Challenges").Append(&challenges).Error; err != nil {
		return err
	}
	return nil
}

func (this *ListGroups) GetAllGroup() error {
	db := OpenDB()
	defer db.Close()

	if err := db.Find(&this).Error; err != nil {
		return err
	}

	return nil
}

func (this *Group) GetAccountLimit(begin, limit int) error {
	db := OpenDB()
	defer db.Close()

	if err := db.Find(&this).Error; err != nil {
		return err
	}

	return db.Model(&this).Offset(begin).Limit(limit).Association("Accounts").Find(&this.Accounts).Error
}

func (this *Group) DeleteGroup() error {
	db := OpenDB()
	defer db.Close()

	db.First(&this)

	if err := db.Model(&this).Association("Accounts").Clear().Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Challenges").Clear().Error; err != nil {
		return err
	}

	return db.Unscoped().Where("id = ?", this.ID).Delete(&this).Error
}

func (this *Group) GetAccountOfGroup() error {
	db := OpenDB()
	defer db.Close()

	if err := db.Find(&this).Error; err != nil {
		return err
	}

	return db.Model(&this).Association("Accounts").Find(&this.Accounts).Error
}

func (this *Group) GetChallengOfGroup() error {
	db := OpenDB()
	defer db.Close()

	if err := db.Find(&this).Error; err != nil {
		return err
	}

	return db.Model(&this).Association("Challenges").Find(&this.Challenges).Error
}
