package models

import (
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type ResultOfSearch struct {
	Topics     ListTopics
	Challenges ListChallenges
	Problems   ListProblems
}

func SearchForKeyword(keyword string, premium int) (ResultOfSearch, error) {
	db := OpenDB()
	defer db.Close()

	data := ResultOfSearch{}

	if err := db.Where("name_topic like ?", "%"+keyword+"%").Find(&data.Topics).Error; err != nil {
		return data, err
	}

	if err := db.Where("name like ?", "%"+keyword+"%").Find(&data.Challenges).Error; err != nil {
		return data, err
	}

	if premium == 1 {
		if err := db.Where("name_problem like ?", "%"+keyword+"%").Find(&data.Problems).Error; err != nil {
			return data, err
		}
	} else {
		if err := db.Where("name_problem like ? and premium = ?", "%"+keyword+"%", premium).Find(&data.Problems).Error; err != nil {
			return data, err
		}
	}

	return data, nil
}
