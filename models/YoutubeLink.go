package models

import (
	"github.com/jinzhu/gorm"
)

type YoutubeLink struct {
	gorm.Model
	Link      string `json:"youtube_link" gorm:"type:TEXT"`
	IDProblem uint   `json:"id_problem" gorm:"foreignkey:problem"`
}

type ListYoutubeLinks []YoutubeLink

func GetLinksYoutube(problem Problem) (ListYoutubeLinks, error) {
	db := OpenDB()
	defer db.Close()

	listYoutube := ListYoutubeLinks{}
	err := db.Where("id_problem = ?", problem.ID).Find(&listYoutube).Error
	if err != nil {
		return nil, err
	}
	return listYoutube, nil
}

func DeleteLinkYoutube(problem Problem) error {
	db := OpenDB()
	defer db.Close()

	return db.Unscoped().Where("id_problem = ?", problem.ID).Delete(YoutubeLink{}).Error
}

func (this *YoutubeLink) CreateYoutubeLink() error {
	db := OpenDB()
	defer db.Close()

	return db.Debug().Create(&this).Error
}
