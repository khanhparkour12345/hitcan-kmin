package models

import "github.com/jinzhu/gorm"

type VideosOfLesson struct {
	gorm.Model
	Iframe   string `json:"iframe,omitempty" gorm:"type:TEXT"`
	IDLesson uint   `json:"id_lesson, omitempty" gorm:"foreignkey:lessons"`
}

func (this *VideosOfLesson) Create() {
	db := OpenDB()
	defer db.Close()

	db.Create(&this)
}

func GetAllVideosByLessonID(idLesson uint) []VideosOfLesson {
	db := OpenDB()
	defer db.Close()

	var videos []VideosOfLesson
	db.Model(&videos).Where("id_lesson = ?", idLesson).Find(&videos)
	return videos
}
