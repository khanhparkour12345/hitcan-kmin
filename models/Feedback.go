package models

import (
	"github.com/jinzhu/gorm"
)

type FeedBack struct {
	gorm.Model
	IDUser    uint `json:"id_user" gorm:"not null"`
	IDProblem uint `json:"id_problem" gorm:"not null"`

	// 0 là chưa hiều, 1 là hiểu sơ sơ, 2 là rất hiểu
	Understanding int    `json:"understanding" gorm:"not null"`
	Comment       string `json:"comment,omitempty" gorm:"type:TEXT"`
	Seen          bool   `json:"seen" gorm:"default:false"`
}

type ListFeedbacks []FeedBack

func (this *FeedBack) CreateFeedBack() error {
	db := OpenDB()
	defer db.Close()

	return db.Create(&this).Error
}

func (this *FeedBack) GetFeedBack() error {
	db := OpenDB()
	defer db.Close()

	return db.Where("id_user = ? and id_problem = ?", this.IDUser, this.IDProblem).Find(&this).Error
}

func (this *ListFeedbacks) GetFeedbackLimit(begin, limit int) error {
	db := OpenDB()
	defer db.Close()

	return db.Order("created_at DESC").Offset(begin).Limit(limit).Find(&this).Error
}

func (this *FeedBack) DeleteFeedBack() error {
	db := OpenDB()
	defer db.Close()

	return db.Unscoped().Where("id_user = ? and id_problem = ?", this.IDUser, this.IDProblem).Delete(&this).Error
}

func (this *ListFeedbacks) CountingFeedback() int {
	db := OpenDB()
	defer db.Close()

	return int(db.Find(&this).RowsAffected)
}

func (this *ListFeedbacks) CoutingUnderstand() int {
	db := OpenDB()
	defer db.Close()

	return int(db.Where("understanding = ?", 2).Find(this).RowsAffected)
}

func (this *ListFeedbacks) CoutingNormal() int {
	db := OpenDB()
	defer db.Close()

	return int(db.Where("understanding = ?", 1).Find(this).RowsAffected)
}

func (this *ListFeedbacks) CoutingDontUnderstand() int {
	db := OpenDB()
	defer db.Close()

	return int(db.Where("understanding = ?", 0).Find(this).RowsAffected)
}

func GetNumberOfFeedbackNotSeen() int {
	db := OpenDB()
	defer db.Close()

	temp := FeedBack{}
	return int(db.Where("seen = ?", false).Find(&temp).RowsAffected)
}

func (this *FeedBack) UpdateFeedback() error {
	db := OpenDB()
	defer db.Close()

	return db.Model(&this).Updates(&this).Error
}
