package models

import (
	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
)

type Course struct {
	gorm.Model
	Name        string `json:"name" gorm:"unique"`
	Description string `json:"description" gorm:"type:VARCHAR(135)"`
	SearchURL   string `json:"searchURL" gorm:"unique"`
	Image       string `json:"image" gorm:"type:TEXT"`
	ImageLocal  string `json:"image_local" gorm:"type:TEXT"`
	QRCode      string `json:"qr_code" gorm:"unique"`

	Accounts []Account `json:"accounts_in_course,omitempty" gorm:"many2many:account_in_course;"`
	Chapters []Chapter `json:"chapter_in_course,omitempty" gorm:"foreignkey:IDcourse;association_foreignkey:ID"`
}

func (this *Course) Create(accounts []Account) error {
	db := OpenDB()
	defer db.Close()

	this.SearchURL = slug.MakeLang(this.Name, "en")
	this.QRCode = GenerateQR()

	if err := db.Create(&this).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Accounts").Append(&accounts).Error; err != nil {
		return err
	}

	for _, val := range accounts {
		val.Premium = 1
		if err := val.UpdatePremium(); err != nil {
			return err
		}
	}

	return nil
}

func (this *Course) UpdateCourseByID(accounts []Account) error {
	db := OpenDB()
	defer db.Close()

	if err := db.Model(&this).Where("id = ?", this.ID).Update(&this).Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Accounts").Clear().Error; err != nil {
		return err
	}

	if err := db.Model(&this).Association("Accounts").Append(&accounts).Error; err != nil {
		return err
	}

	for _, val := range accounts {
		val.Premium = 1
		if err := val.UpdatePremium(); err != nil {
			return err
		}
	}

	return nil
}

func (this *Course) GetDetailCourseBySearchURLAnName(isGetAccount bool) {
	db := OpenDB()
	defer db.Close()

	db.Where("search_url like ? or name like ?", "%"+this.SearchURL+"%", "%"+this.Name+"%").First(&this)
	if isGetAccount {
		db.Model(&this).Association("Accounts").Find(&this.Accounts)
	}
	this.Chapters = GetAllChapterByCourseID(this.ID)
}

func (this *Course) GetDetailCourseByID(isGetAccount bool) {
	db := OpenDB()
	defer db.Close()

	db.Where("id = ?", this.ID).Find(&this)
	if isGetAccount {
		db.Model(&this).Association("Accounts").Find(&this.Accounts)
	}
	this.Chapters = GetAllChapterByCourseID(this.ID)
}

func GetAllCourse() []Course {
	db := OpenDB()
	defer db.Close()

	var courses []Course
	db.Find(&courses)

	return courses
}

func (this *Course) DeleteCourseByID() error {
	db := OpenDB()
	defer db.Close()

	db.Where("id = ?", this.ID).Find(&this)

	if err := db.Model(&this).Association("Accounts").Clear().Error; err != nil {
		return err
	}

	return db.Unscoped().Where("id = ?", this.ID).Delete(&this).Error
}

func (this *Course) GetAccountOfCourse() {
	db := OpenDB()
	defer db.Close()

	db.Where("id = ?", this.ID).Find(&this)
	db.Model(&this).Association("Accounts").Find(&this.Accounts)
}
