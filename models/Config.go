package models

import (
	"math/rand"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	CONNECTION_STRING = os.Getenv("ConnectionStringHitCan")
	db                *gorm.DB
)

func OpenDB() *gorm.DB {
	if db == nil {
		db, err := gorm.Open("mysql", CONNECTION_STRING)
		if err != nil {
			panic(err)
		}
		return db
	}

	return db
}

func GenerateQR() string {
	rand.Seed(time.Now().UnixNano())
	str := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	min := 0
	max := len(str)

	code := ""
	for i := 0; i < 10; i++ {
		idx := rand.Intn(max-min) + min
		code += string(str[idx])
	}

	return code
}

func MigrateAll() {
	db := OpenDB()
	defer db.Close()

	db.AutoMigrate(
		&Account{},
		&Topic{},
		&Challenge{},
		&Problem{},
		&InputOutput{},
		&YoutubeLink{},
		&FeedBack{},
		&Group{},
		&Course{},
		&Chapter{},
		&Lesson{},
		&VideosOfLesson{},
		&Document{},
		&Chapter{},
	)
}
