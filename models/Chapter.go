package models

import (
	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
)

type Chapter struct {
	gorm.Model
	Name      string   `json:"name,omitempty" gorm:"type:VARCHAR(100)"`
	SearchURL string   `json:"search_url,omitempty" gorm:"type:VARCHAR(100)"`
	IDCourse  uint     `json:"id_course,omitempty" gorm:"foreignkey:courses"`
	Lessons   []Lesson `json:"lessons,omitempty" gorm:"foreignkey:IDChapter;association_foreignkey:ID"`
}

func (this *Chapter) Create() {
	db := OpenDB()
	defer db.Close()

	this.SearchURL = slug.MakeLang(this.Name, "en")

	db.Create(&this)
}

func GetAllChapterByCourseID(idCourse uint) []Chapter {
	db := OpenDB()
	defer db.Close()

	var chapters []Chapter
	db.Model(&chapters).Where("id_course = ?", idCourse).Find(&chapters)
	for i, chapter := range chapters {
		chapter.Lessons = GetAllLessonByChapterID(chapter.ID)
		chapters[i] = chapter
	}

	return chapters
}
