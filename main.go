package main

import (
	router "hitcan-kmin/router"
)

func main() {
	router.MigrateDB()
	router.HandlerTemplate()
}
