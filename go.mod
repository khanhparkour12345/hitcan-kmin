module hitcan-kmin

go 1.13

require (
	github.com/GeertJohan/go.rice v1.0.0
	github.com/appleboy/gin-jwt v2.5.0+incompatible
	github.com/auth0/go-jwt-middleware v0.0.0-20190805220309-36081240882b // indirect
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/foolin/gin-template v0.0.0-20190415034731-41efedfb393b
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
	github.com/gin-gonic/contrib v0.0.0-20191209060500-d6e26eeaa607 // indirect
	github.com/gin-gonic/gin v1.5.0
	github.com/gorilla/handlers v1.4.2 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/gorilla/websocket v1.4.1
	github.com/gosimple/slug v1.9.0
	github.com/huandu/facebook v2.3.1+incompatible // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/thinkerou/favicon v0.1.0
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
