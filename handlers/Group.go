package handlers

import (
	"fmt"
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func DeleteGroup(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println(id, email)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	idGroup, err := strconv.Atoi(c.Query("id_group"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	group := models.Group{}
	group.ID = uint(idGroup)
	if err := group.DeleteGroup(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func GetAllGroup(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println(id, email)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	groups := models.ListGroups{}
	if err := groups.GetAllGroup(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"groups": groups,
		},
	})
}

func CreateGroup(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println(id, email)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	data := models.Group{}
	if err := c.ShouldBind(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	accounts := []models.Account{}
	for _, val := range data.Accounts {
		user := models.Account{}
		user.ID = uint(val.ID)
		if err := user.GetAccount(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Hitcan["error"],
				},
			})
			return
		}
		accounts = append(accounts, user)
	}

	challenges := []models.Challenge{}
	for _, val := range data.Challenges {
		challenge := models.Challenge{}
		challenge.ID = val.ID
		if err := challenge.GetChallenge(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Hitcan["error"],
				},
			})
			return
		}
		challenges = append(challenges, challenge)
	}

	data.Accounts = accounts
	data.Challenges = challenges
	if err := data.CreateGroup(accounts, challenges); err != nil {
		fmt.Println("Hay o day")
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"group": data,
		},
	})
}

func UpdateGroup(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println(id, email)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	data := models.Group{}
	if err := c.ShouldBind(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	accounts := []models.Account{}
	for _, val := range data.Accounts {
		user := models.Account{}
		user.ID = uint(val.ID)
		if err := user.GetAccount(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Hitcan["error"],
				},
			})
			return
		}
		accounts = append(accounts, user)
	}

	challenges := []models.Challenge{}
	for _, val := range data.Challenges {
		challenge := models.Challenge{}
		challenge.ID = val.ID
		if err := challenge.GetChallenge(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Hitcan["error"],
				},
			})
			return
		}
		challenges = append(challenges, challenge)
	}

	data.Accounts = accounts
	data.Challenges = challenges
	if err := data.UpdateGroup(accounts, challenges); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"group": data,
		},
	})
}

func GetAccountOfGroup(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println(id, email)
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	idGroup, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	data := models.Group{}
	data.ID = uint(idGroup)
	if err := data.GetAccountOfGroup(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	if err := data.GetChallengOfGroup(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"group": data,
		},
	})
}
