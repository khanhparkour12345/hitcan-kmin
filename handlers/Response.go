package handlers

var (
	Account = map[string]string{
		"not_match":           "Dữ liệu không hợp lệ",
		"email_not_match":     "Email không hợp lệ",
		"phone_not_match":     "Số điện thoại không hợp lệ",
		"error":               "Lỗi khi tạo tài khoản!",
		"email_exist":         "Email đã được sử dụng",
		"can_not_send_email":  "Không thể gửi email xác thực!",
		"not_match_login":     "Email hoặc password không khớp!",
		"err_update_password": "Lỗi khi cập nhật email",
		"err_update":          "Lỗi khi cập nhật dữ liệu",
		"not_login":           "Bạn phải đăng nhập để thực hiện chức năng này!",
	}

	Challenge = map[string]string{
		"error":         "Không xử lý được yêu cầu",
		"no_data_found": "Không tìm thấy thử thách này",
	}

	Problem = map[string]string{
		"error":         "Không xử lý được yêu cầu",
		"no_data_found": "Không tìm thấy thử thách này",
	}

	Topic = map[string]string{
		"error":         "Không xử lý được yêu cầu",
		"no_data_found": "Không tìm thấy chủ đề",
	}

	Hitcan = map[string]string{
		"error":             "Không xử lý được yêu cầu",
		"not_authorization": "Lỗi xác thực",
	}

	Google = map[string]string{
		"error": "Không thể xác thực",
		"exist": "tài khoản này đã tồn tại",
	}

	Facebook = map[string]string{
		"error": "Không thể xác thực",
		"exist": "tài khoản này đã tồn tại",
	}
)
