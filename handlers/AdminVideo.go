// Package handlers provides ...
package handlers

import (
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func ComsumDataForAdminAddVideo() ([]models.Course, []models.Chapter, []models.Lesson) {
	courses := models.GetAllCourse()
	var chapters []models.Chapter
	var lessons []models.Lesson
	if len(courses) > 0 {
		chapters = models.GetAllChapterByCourseID(courses[0].ID)
		if len(chapters) > 0 {
			lessons = models.GetAllLessonByChapterID(chapters[0].ID)
		}
	}
	return courses, chapters, lessons
}

func GetChapterByCourseID(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	idCourse, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	chapters := models.GetAllChapterByCourseID(uint(idCourse))
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": chapters,
	})
}

func GetLessonByChapterID(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	idChapter, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	lessons := models.GetAllLessonByChapterID(uint(idChapter))
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": lessons,
	})
}

func UploadVideosLesson(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	videos := []models.VideosOfLesson{}
	if err := c.ShouldBind(&videos); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	for _, video := range videos {
		video.Create()
	}
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": "success",
	})
}
