package handlers

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hitcan-kmin/models"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func generateStateGoogleCookie() string {
	b := make([]byte, 32)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	return state
}

func OauthGoogleLogin(c *gin.Context) {
	oauthState := generateStateGoogleCookie()
	u := GetConfigGoogle().AuthCodeURL(oauthState)

	c.Redirect(http.StatusTemporaryRedirect, u)
}

func GetConfigGoogle() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     GOOGLE_CLIENT_ID,
		ClientSecret: GOOGLE_CLIENT_SECRET,
		RedirectURL:  GOOGLE_CALLBACK,
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
		},
		Endpoint: google.Endpoint,
	}
}

func GetUserDataFromGoogle(code string) ([]byte, error) {
	token, err := GetConfigGoogle().Exchange(context.Background(), code)
	if err != nil {
		return nil, err
	}

	response, err := http.Get(OAUTH_GOOGLE_URL_API + token.AccessToken)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return contents, nil
}

func GoogleCallBack(c *gin.Context) {
	data, err := GetUserDataFromGoogle(c.Query("code"))
	if err != nil {
		fmt.Println(err)
		c.Redirect(http.StatusTemporaryRedirect, "http://hit.kmin.edu.vn/page_not_found")
		return
	}

	var info gin.H
	if err := json.Unmarshal(data, &info); err != nil {
		fmt.Println(err)
		c.Redirect(http.StatusTemporaryRedirect, "http://hit.kmin.edu.vn/page_not_found")
		return
	}

	name := info["name"].(string)
	id := info["id"].(string)
	avatar := info["picture"].(string)
	email := info["email"].(string)
	LoginGoogle(c, email, name, id, avatar)
}

func LoginGoogle(c *gin.Context, email, name, id, avatar string) {
	account := models.Account{
		Email:     email,
		Name:      name,
		GoogleID:  id,
		URLAvatar: avatar,
	}

	if !account.CheckIDGoogleExist() {
		if err := account.CreateAccount(); err != nil {
			fmt.Println(err)
			c.Redirect(http.StatusTemporaryRedirect, "http://hit.kmin.edu.vn/page_not_found")
			return
		}
	} else {

		if account.LocationAvatar != "" {
			account.LocationAvatar = ""
			account.URLAvatar = ""
		}

		if err := account.UpdateWithEmail(); err != nil {
			fmt.Println(err)
			c.Redirect(http.StatusTemporaryRedirect, "http://hit.kmin.edu.vn/page_not_found")
			return
		}
	}

	tokenString, err := GenToken(account)
	if err != nil {
		fmt.Println(err)
		c.Redirect(http.StatusTemporaryRedirect, "http://hit.kmin.edu.vn/page_not_found")
		return
	}

	expiration := time.Now().Add(365 * 24 * time.Hour)

	cookie := http.Cookie{
		Name:    "Authorization",
		Value:   tokenString,
		Expires: expiration,
		Path:    "/",
	}
	http.SetCookie(c.Writer, &cookie)

	if account.Premium == 2 {
		c.Redirect(http.StatusSeeOther, "/admin_index")
		return
	}

	c.Redirect(http.StatusSeeOther, "/")
	return
}
