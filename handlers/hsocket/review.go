package hsocket

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var HHub *Hub

type reviewHub struct {
	clients   map[*websocket.Conn]bool
	Broadcast chan interface{}
}

var ReviewHub = reviewHub{
	clients:   make(map[*websocket.Conn]bool),
	Broadcast: make(chan interface{}),
}

func HandleSocket(c *gin.Context) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: HHub, conn: conn, send: make(chan interface{})}
	client.hub.Register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
}
