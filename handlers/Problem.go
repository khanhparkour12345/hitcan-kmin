package handlers

import (
	"fmt"
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetProblem(c *gin.Context) {
	problemID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["error"],
			},
		})
		return
	}

	problem := models.Problem{}
	problem.ID = uint(problemID)
	if err := problem.GetProblem(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["no_data_found"],
			},
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": problem,
	})
}

func GetNumberOfProblems(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	var problems models.ListProblems
	number := problems.CoutingListProblem()
	c.JSON(http.StatusOK, gin.H{
		"code":               http.StatusOK,
		"number_of_problems": number,
	})
}

func GetAllProblems(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	list, err := models.GetAllProblem()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"problems": list,
		},
	})
}

func GetProblemByChallengeID(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	idChallenge, err := strconv.Atoi(c.Query("id_challenge"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["error"],
			},
		})
		return
	}

	list := models.ListProblems{}
	if err := list.GetProblemByChallengeID(idChallenge); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"problems": list,
		},
	})
}

func CreateProblem(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	type dataProblem struct {
		Topic          models.Topic       `json:"topic"`
		ListChallenges []models.Challenge `json:"challenges"`
		Problem        models.Problem     `json:"problem"`
	}

	dataBody := dataProblem{}
	if err := c.ShouldBind(&dataBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["error"],
			},
		})
		return
	}

	if !dataBody.Topic.CheckExist() {
		if err := dataBody.Topic.CreateTopic(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Problem["error"],
				},
			})
			return
		}
	} else {
		if err := dataBody.Topic.GetTopic(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Problem["error"],
				},
			})
			return
		}
	}

	for i, val := range dataBody.ListChallenges {
		if !val.CheckExist() {
			val.IDTopic = dataBody.Topic.ID
			if err := val.CreateChallenge(); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"code": http.StatusBadRequest,
					"error": gin.H{
						"message": Problem["error"],
					},
				})
				return
			}
			dataBody.ListChallenges[i] = val
		} else {
			if err := val.GetChallengeByID(); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"code": http.StatusBadRequest,
					"error": gin.H{
						"message": Problem["error"],
					},
				})
				return
			}
		}
		dataBody.ListChallenges[i] = val
	}

	dataBody.Problem.Challenges = dataBody.ListChallenges
	if err := dataBody.Problem.CreateProblem(dataBody.ListChallenges); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func UpdateProblem(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	type dataProblem struct {
		Topic          models.Topic       `json:"topic"`
		ListChallenges []models.Challenge `json:"challenges"`
		Problem        models.Problem     `json:"problem"`
	}

	dataBody := dataProblem{}
	if err := c.ShouldBind(&dataBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["error"],
			},
		})
		return
	}

	fmt.Println(dataBody.Problem)
	if err := dataBody.Problem.RemoveRelationShipProblem(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Problem["error"],
			},
		})
		return
	}

	if !dataBody.Topic.CheckExist() {
		if err := dataBody.Topic.CreateTopic(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Problem["error"],
				},
			})
			return
		}
	} else {
		if err := dataBody.Topic.GetTopic(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"message": Problem["error"],
				},
			})
			return
		}
	}

	for i, val := range dataBody.ListChallenges {
		if !val.CheckExist() {
			val.IDTopic = dataBody.Topic.ID
			if err := val.CreateChallenge(); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"code": http.StatusBadRequest,
					"error": gin.H{
						"message": Problem["error"],
					},
				})
				return
			}
			dataBody.ListChallenges[i] = val
		} else {
			if err := val.GetChallengeByID(); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"code": http.StatusBadRequest,
					"error": gin.H{
						"message": Problem["error"],
					},
				})
				return
			}
		}
		dataBody.ListChallenges[i] = val
	}

	dataBody.Problem.Challenges = dataBody.ListChallenges
	if err := dataBody.Problem.UpdateProblem(dataBody.ListChallenges); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Problem["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func DeleteProblem(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	type dataMustGet struct {
		ListID []uint `json:"list_id"`
	}

	data := dataMustGet{}

	if err := c.ShouldBind(&data); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}

	for _, id := range data.ListID {
		problem := models.Problem{}
		problem.ID = id
		if err := problem.DeleteProblemById(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"error":   err.Error(),
					"message": Problem["error"],
				},
			})
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}
