package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func DeleteCookie(c *gin.Context, name, path string, httpOnly bool) {
	cookie := &http.Cookie{
		Name:     name,
		Value:    "",
		Path:     path,
		MaxAge:   -1,
		HttpOnly: httpOnly,
	}

	http.SetCookie(c.Writer, cookie)
}
