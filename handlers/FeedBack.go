package handlers

import (
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func CreateNewFeedBack(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_login"],
			},
		})
		return
	}

	idProblem, err := strconv.Atoi(c.Param("id_problem"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	newFeedBack := models.FeedBack{}
	if err := c.ShouldBind(&newFeedBack); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	newFeedBack.IDUser = account.ID
	newFeedBack.IDProblem = uint(idProblem)

	if err := newFeedBack.CreateFeedBack(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Hitcan["error"],
			},
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
	})
}

func GetNumberOfFeedback(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	var feedbacks = models.ListFeedbacks{}
	number := feedbacks.CountingFeedback()
	c.JSON(http.StatusOK, gin.H{
		"code":                http.StatusOK,
		"number_of_feedbacks": number,
	})
}

func GetNumberOfFeedbackNotSeen(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	feedbackNotSeen := models.GetNumberOfFeedbackNotSeen()
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"number_feedback_notseen": feedbackNotSeen,
		},
	})
}

func GetFeedBackDetailChart(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}
	var feedbacks = models.ListFeedbacks{}
	numberDontUnderstand := feedbacks.CoutingDontUnderstand()
	numberNormal := feedbacks.CoutingNormal()
	numberUnderstand := feedbacks.CoutingUnderstand()

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"number_dont_understand": numberDontUnderstand,
			"number_normal":          numberNormal,
			"number_understand":      numberUnderstand,
		},
	})

}

func GetFeedBackLimit(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	listFeedbacks := models.ListFeedbacks{}
	limit := 7
	allFeedback := listFeedbacks.CountingFeedback()
	_, begin := PaginationHandler(c, limit)
	numOfPages := allFeedback / limit
	numOfPages++

	if err := listFeedbacks.GetFeedbackLimit(begin, limit); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code":  http.StatusBadRequest,
			"error": err.Error(),
		})
		return
	}

	type objectInData struct {
		Feedback models.FeedBack `json:"feedback,omitempty"`
		Account  models.Account  `json:"account,omitempty"`
		Problem  models.Problem  `json:"problem,omitempty"`
	}
	type dataResponse struct {
		ListFeedback []objectInData `json:"list_feedback,omitempty"`
	}

	dataRes := dataResponse{}
	for _, val := range listFeedbacks {
		obj := objectInData{}
		feedback := models.FeedBack{}
		feedback = val
		problem := models.Problem{
			Model: gorm.Model{
				ID: feedback.IDProblem,
			},
		}
		account := models.Account{
			Model: gorm.Model{
				ID: feedback.IDUser,
			},
		}
		if err := problem.GetProblemByID(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code":  http.StatusBadRequest,
				"error": err.Error(),
			})
			return
		}
		if err := account.GetAccount(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code":  http.StatusBadRequest,
				"error": err.Error(),
			})
			return
		}
		obj.Account = account
		obj.Problem = problem
		obj.Feedback = feedback
		dataRes.ListFeedback = append(dataRes.ListFeedback, obj)
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": gin.H{
			"list":     dataRes,
			"max_page": numOfPages,
		},
	})
}

func GetDetailFeedback(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	if account.Premium < 2 {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"message": Account["not_match"],
			},
		})
		return
	}

	type dataRequest struct {
		IDAccount  uint `json:"id_account"`
		IDProblem  uint `json:"id_problem"`
		IDFeedback uint `json:"id_feedback"`
	}

	dataBody := dataRequest{}

	if err := c.ShouldBind(&dataBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}

	type objectInData struct {
		Feedback models.FeedBack `json:"feedback,omitempty"`
		Account  models.Account  `json:"account,omitempty"`
		Problem  models.Problem  `json:"problem,omitempty"`
	}

	feedback := models.FeedBack{}
	accountFeedback := models.Account{}
	problemFeedback := models.Problem{}
	feedback.ID = dataBody.IDFeedback
	accountFeedback.ID = dataBody.IDAccount
	problemFeedback.ID = dataBody.IDProblem

	if err := accountFeedback.GetAccount(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}

	if err := problemFeedback.GetProblemByID(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":   err.Error(),
				"message": Hitcan["error"],
			},
		})
		return
	}

	feedback.IDProblem = problemFeedback.ID
	feedback.IDUser = accountFeedback.ID
	if err := feedback.GetFeedBack(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": http.StatusBadRequest,
			"error": gin.H{
				"error":              err.Error(),
				"line_error_backend": "456",
				"message":            Hitcan["error"],
			},
		})
		return
	}

	if feedback.Seen == false {
		feedback.Seen = true
		if err := feedback.UpdateFeedback(); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"code": http.StatusBadRequest,
				"error": gin.H{
					"error":              err.Error(),
					"line_error_backend": "470",
					"message":            Hitcan["error"],
				},
			})
			return
		}
	}

	dataResponse := objectInData{
		Feedback: feedback,
		Account:  accountFeedback,
		Problem:  problemFeedback,
	}

	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"data": dataResponse,
	})
}
