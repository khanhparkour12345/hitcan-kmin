const getCookie = (cname) => {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; ++i) {
        let c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

(function ($) {
    // USE STRICT
    "use strict";

    // Dropdown
    try {
        var menu = $(".js-item-menu");
        var sub_menu_is_showed = -1;

        for (var i = 0; i < menu.length; i++) {
            $(menu[i]).on("click", function (e) {
                e.preventDefault();
                $(".js-right-sidebar").removeClass("show-sidebar");
                if (jQuery.inArray(this, menu) == sub_menu_is_showed) {
                    $(this).toggleClass("show-dropdown");
                    sub_menu_is_showed = -1;
                } else {
                    for (var i = 0; i < menu.length; i++) {
                        $(menu[i]).removeClass("show-dropdown");
                    }
                    $(this).toggleClass("show-dropdown");
                    sub_menu_is_showed = jQuery.inArray(this, menu);
                }
            });
        }
        $(".js-item-menu, .js-dropdown").click(function (event) {
            event.stopPropagation();
        });

        $("body,html").on("click", function () {
            for (var i = 0; i < menu.length; i++) {
                menu[i].classList.remove("show-dropdown");
            }
            sub_menu_is_showed = -1;
        });
    } catch (error) {
        console.log(error);
    }

    var wW = $(window).width();
    // Right Sidebar
    var right_sidebar = $(".js-right-sidebar");
    var sidebar_btn = $(".js-sidebar-btn");

    sidebar_btn.on("click", function (e) {
        e.preventDefault();
        for (var i = 0; i < menu.length; i++) {
            menu[i].classList.remove("show-dropdown");
        }
        sub_menu_is_showed = -1;
        right_sidebar.toggleClass("show-sidebar");
    });

    $(".js-right-sidebar, .js-sidebar-btn").click(function (event) {
        event.stopPropagation();
    });

    $("body,html").on("click", function () {
        right_sidebar.removeClass("show-sidebar");
    });

    // Sublist Sidebar
    try {
        var arrow = $(".js-arrow");
        arrow.each(function () {
            var that = $(this);
            that.on("click", function (e) {
                e.preventDefault();
                that.find(".arrow").toggleClass("up");
                that.toggleClass("open");
                that.parent().find(".js-sub-list").slideToggle("250");
            });
        });
    } catch (error) {
        console.log(error);
    }

    try {
        // Hamburger Menu
        $(".hamburger").on("click", function () {
            $(this).toggleClass("is-active");
            $(".navbar-mobile").slideToggle("500");
        });
        $(".navbar-mobile__list li.has-dropdown > a").on("click", function () {
            var dropdown = $(this).siblings("ul.navbar-mobile__dropdown");
            $(this).toggleClass("active");
            $(dropdown).slideToggle("500");
            return false;
        });
    } catch (error) {
        console.log(error);
    }
})(jQuery);

$("#logout").on("click", () => {
    axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/logout",
    }).then(() => {
        document.location = "/";
    });
});

// var socket = io.connect("ws://hitcan.kmin.edu.vn/ws/noti/feedback");
// console.log(socket);

// socket.addEventListener("open", function(e) {
//     console.log(e);
//     console.log("hello socket");
// });

// socket.addEventListener("error", function(e) {
//     console.log(e);
//     console.log("error");
// });

// socket.addEventListener("message", function(e) {
//     console.log(e.data);
// });

// Notification.requestPermission();

// const notifyMe = (titleNoti, bodyNoti, direction) => {
//     if (!("Notification" in window)) {
//         console.error("this browser does not support system notification");
//     } else if (Notification.permission === "granted") {
//         notifyDetail(titleNoti, bodyNoti, direction);
//     } else if (Notification.permission === "denied") {
//         Notification.requestPermission(permission => {
//             if (permission === "granted") {
//                 notifyDetail(titleNoti, bodyNoti, direction);
//             }
//         });
//     }
// };

// const notifyDetail = (titleNoti, bodyNoti, direction) => {
//     let notification = new Notification(titleNoti, {
//         icon: "http://hit.kmin.edu.vn/img/logo_kmin.png",
//         body: bodyNoti
//     });

//     notification.onclick = function() {
//         window.open("http://hit.kmin.edu.vn" + direction);
//     };

//     setTimeout(notification.close.bind(notification), 7000);
// };
