let load = `<div class="preloader-wrapper small active">
  <div class="spinner-layer spinner-yellow-only">
    <div class="circle-clipper left">
      <div class="circle"></div>
    </div><div class="gap-patch">
      <div class="circle"></div>
    </div><div class="circle-clipper right">
      <div class="circle"></div>
    </div>
  </div>
  </div>`;

let oldEmail = $("#email").val();

$(document).ready(function () {
    let mediumRegex = new RegExp("((?=.*[a-z])(?=.*[0-9])(?=.{6,}))");
    let strongPassword = new RegExp("((?=.*[a-z])(?=.*[0-9])(?=.{8,}))");
    let phoneRegex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    let emailRegex = /^\S+@\S+$/g;
    let cropp;
    const view = $("#img-preview")[0];

    $(".modal").modal();
    $(".materialboxed").materialbox();

    $("#avatar-file").on("change", function (e) {
        let files = e.target.files;
        let done = function (url) {
            $("#avatar-file").val("");
            $("#img-preview").attr("src", url);
            $("#edit-avatar-popup").modal("open");
            cropp = new Cropper(view, {
                aspectRatio: 1,
                viewMode: 3,
            });
        };
        let reader;
        let file;
        let url;

        if (files && files.length > 0) {
            file = files[0];

            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function (e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $("#cut-and-save-avatar").on("click", () => {
        $("#cut-and-save-avatar").html("").append(load);
        let canvas;

        if (cropp) {
            canvas = cropp.getCroppedCanvas({
                width: 300,
                height: 300,
            });
            canvas.toBlob(function (blob) {
                let reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    let data = reader.result;
                    updateAvatar(data);
                    cropp.destroy();
                    cropp = null;
                };
            });
        }
    });

    $("#delete-avatar").on("click", () => {
        let header = getCookie("Authorization");
        axios({
            method: "put",
            url: "http://hit.kmin.edu.vn/account/api/delete_avatar",
            headers: {
                Authorization: header,
            },
        })
            .then((res) => {
                $("#avatar-image").attr("src", res.data.url_image);
                $("#avatar-navbar").attr("src", res.data.url_image);
                $("#avatar-sidebar").attr("src", res.data.url_image);
                $("#edit-avatar-popup").modal("close");
                $("#cut-and-save-avatar").html("Lưu");
                $(".preload-wrapper").remove();
            })
            .catch((err) => {
                $("#cut-and-save-avatar").html("Lưu");
                $(".preload-wrapper").remove();
                let toastHTML = `<i class="material-icons">
              sentiment_very_dissatisfied
              </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${
                  err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
              }</span></span>`;
                M.toast({ html: toastHTML });
            });
    });

    let flagname = true;
    let flagmail = true;
    let flagphone = true;
    $("#name").on("input", () => {
        let value = $("#name").val();
        value = value.trim();
        if (value != "") {
            flagname = true;
            $("#name").removeClass("validate").removeClass("invalid").addClass("valid");
        } else {
            flagname = false;
            $("#name").removeClass("validate").removeClass("valid").addClass("invalid");
        }
    });

    $("#email").on("input", () => {
        let emailValue = $("#email").val();
        if (emailRegex.test(emailValue)) {
            flagmail = true;
        } else {
            flagmail = false;
        }
    });

    $("#phone").on("input", () => {
        let phoneNumber = $("#phone").val();
        if (phoneRegex.test(phoneNumber)) {
            flagphone = true;
            $("#phone").removeClass("validate").removeClass("invalid").addClass("valid");
        } else {
            flagphone = false;
            $("#phone").removeClass("validate").removeClass("valid").addClass("invalid");
        }
    });

    $("#save-profile").on("click", () => {
        if (flagmail == true && flagname == true && flagphone == true) {
            let mail = $("#email").val();
            let name = $("#name").val();
            let phone = $("#phone").val();
            let header = getCookie("Authorization");
            $("#save-profile").html("").append(load);

            if (oldEmail !== mail) {
                checkEmailExist(mail, header, name, phone);
            } else {
                axios({
                    method: "put",
                    headers: {
                        Authorization: header,
                    },
                    url: "http://hit.kmin.edu.vn/account/api/update_profile",
                    data: {
                        email: mail,
                        name: name,
                        phone: phone,
                    },
                })
                    .then((res) => {
                        $("#save-profile").html("Lưu");
                        $(".preload-wrapper").remove();
                        success();
                    })
                    .catch((err) => {
                        $("#save-profile").html("Lưu");
                        $(".preload-wrapper").remove();
                        let toastHTML = `<i class="material-icons">
                      sentiment_very_dissatisfied
                      </i><span>&nbsp;&nbsp;&nbsp;&nbsp;${
                          err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                      }</span>`;
                        M.toast({ html: toastHTML });
                    });
            }
        }
    });

    let flag = false;
    $("#password").on("input", () => {
        let oldPass = $("#old-password").val();
        oldPass = oldPass.trim();
        if (flag == true) {
            flag = false;
            removeConfirmPass();
        }
        let inputPass = $("#password").val();
        if (strongPassword.exec(inputPass) && oldPass != "") {
            $("#password").addClass("valid");
            $("#password").removeClass("invalid");
            $("#helper-password").removeClass("medium");
            flag = true;
        } else if (mediumRegex.exec(inputPass) && oldPass != "") {
            $("#password").addClass("invalid");
            $("#helper-password").addClass("medium");
            $("#helper-password").attr("data-error", "medium password");
            flag = true;
        } else {
            $("#password").removeClass("validate");
            $("#password").addClass("invalid");
            $("#helper-password").removeClass("medium");
            $("#helper-password").attr("data-error", "weak password");
            flag = false;
        }
    });

    $("#confirm-password").on("input", () => {
        let confirmPass = $("#confirm-password").val();
        let inputPass = $("#password").val();
        if (confirmPass == inputPass) {
            $("#confirm-password").removeClass("invalid");
            $("#confirm-password").addClass("valid");
            flag = true;
        } else {
            $("#confirm-password").addClass("invalid");
            $("#confirm-password").removeClass("validate");
            flag = false;
        }
    });

    const removeConfirmPass = () => {
        $("#confirm-password").addClass("invalid");
        $("#confirm-password").removeClass("validate");
    };

    $("#save-password").on("click", () => {
        if (flag == true) {
            let oldPass = $("#old-password").val();
            let newPass = $("#confirm-password").val();
            let header = getCookie("Authorization");
            $("#save-password").html("").append(load);

            axios({
                method: "put",
                headers: {
                    Authorization: header,
                },
                url: "http://hit.kmin.edu.vn/account/api/update_password",
                data: {
                    old_password: oldPass,
                    new_password: newPass,
                },
            })
                .then((res) => {
                    $("#save-password").html("Lưu");
                    $(".preload-wrapper").remove();
                    $("#old-password").val("");
                    $("#password").val("");
                    $("#confirm-password").val("");
                    success();
                })
                .catch((err) => {
                    $("#save-password").html("Lưu");
                    $(".preload-wrapper").remove();
                    let toastHTML = `<i class="material-icons">
                  sentiment_very_dissatisfied
                  </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${
                      err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                  }</span></span>`;
                    M.toast({ html: toastHTML });
                });
        }
    });
});

async function updateAvatar(data) {
    let header = getCookie("Authorization");
    axios({
        method: "put",
        url: "http://hit.kmin.edu.vn/account/api/update_avatar",
        headers: {
            Authorization: header,
        },
        data: {
            avatar: data,
        },
    })
        .then((res) => {
            $("#avatar-image").attr("src", res.data.url_image);
            $("#avatar-navbar").attr("src", res.data.url_image);
            $("#avatar-sidebar").attr("src", res.data.url_image);
            $("#edit-avatar-popup").modal("close");
            $("#cut-and-save-avatar").html("Lưu");
            $(".preload-wrapper").remove();
        })
        .catch((err) => {
            $("#cut-and-save-avatar").html("Lưu");
            $(".preload-wrapper").remove();
            let toastHTML = `<i class="material-icons">
              sentiment_very_dissatisfied
              </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${
                  err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
              }</span></span>`;
            M.toast({ html: toastHTML });
        });
}

async function sendMail(email) {
    let code;
    await axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/send_email",
        data: {
            email: email,
        },
    })
        .then((res) => {
            code = res.data.confirm_email;
            $("#confirm-signup").modal("open");
        })
        .catch((err) => {
            let toastHTML = `<i class="material-icons">
      sentiment_very_dissatisfied
      </i><br><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${
          err.response.data ? err.response.data.error.message : "Không thể gửi email"
      }</span></span>`;
            M.toast({ html: toastHTML });
            code = "";
        });
    return code;
}

async function checkEmailExist(email, header, name, phone) {
    await axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/check_email_exist",
        data: {
            email: email,
        },
    })
        .then(() => {
            sendMail(email).then((result) => {
                $("#submit-id-code").on("click", function () {
                    $(this).html("").append(load);
                    let codeInput = $("#id-code").val();
                    codeInput = codeInput.trim();
                    if (codeInput === result) {
                        $(this).html("Xác thực");
                        $(".preload-wrapper").remove();
                        $("#confirm-signup").modal("close");
                        $("#save-profile").html("").append(load);

                        axios({
                            method: "put",
                            headers: {
                                Authorization: header,
                            },
                            url: "http://hit.kmin.edu.vn/account/api/update_profile",
                            data: {
                                email: email,
                                name: name,
                                phone: phone,
                            },
                        })
                            .then((res) => {
                                oldEmail = email;
                                let d = new Date();
                                d.setTime(d.getTime() + 12500 * 24 * 60 * 60 * 1000);
                                document.cookie = `Authorization=${res.data.token}; expires=${d.toGMTString()}; path=/`;
                                $("#save-profile").html("Lưu");
                                $(".preload-wrapper").remove();
                                success();
                            })
                            .catch((err) => {
                                $("#save-profile").html("Lưu");
                                $(".preload-wrapper").remove();
                                let toastHTML = `<i class="material-icons">
                  sentiment_very_dissatisfied
                  </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3"${
                      err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                  }</span></span>`;
                                M.toast({ html: toastHTML });
                            });
                    } else {
                        $("#save-profile").html("Lưu");
                        $(this).html("Xác thực");
                        $(".preload-wrapper").remove();
                        $("#confirm-signup").modal("close");
                        let toastHTML = `<i class="material-icons">
                  sentiment_very_dissatisfied
                  </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3>Không khớp!</span></span>`;
                        M.toast({ html: toastHTML });
                    }
                });
            });
        })
        .catch((err) => {
            $("#save-profile").html("Lưu");
            $(".preload-wrapper").remove();
            let toastHTML = `<i class="material-icons">
      sentiment_very_dissatisfied
      </i><br><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3>${
          err.response.data ? err.response.data.error.message : "Lỗi khi thực hiện yêu cầu!"
      }</span></span>`;
            M.toast({ html: toastHTML });
        });
}
