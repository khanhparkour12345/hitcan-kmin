let countTimeLoad = 1;
let endLoad, nameTopic, idTopic;
let loadOneTime = 0;
let load = `
  <div class="preloader-wrapper active">
    <div class="spinner-layer spinner-yellow-only">
    <div class="circle-clipper left">
      <div class="circle"></div>
    </div><div class="gap-patch">
        <div class="circle"></div>
        </div><div class="circle-clipper right">
      <div class="circle"></div>
    </div>
    </div>
  </div>`;

// hàm render ra những bài tập dễ
const renderExercisesEasy = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 1) {
            $("#exercises-challenge").append(`<div class="card easy">
        <div class="card-content w-50">
          <h4 class="card-title grey-text text-darken-4">
            ${exercisesObj[x].name_challenge}
          </h4>
          <p class="cyan-text darken-4">Dễ</p>
        </div>
        <a
          class="btn btn-large btn-hitchallenge"
          href="/challenge?id=${exercisesObj[x].ID}&name_topic=${nameTopic}&id_topic=${idTopic}"
          ><span>hit challenge</span></a
        >
      </div>`);
        }
    }
};

// hàm render ra những bài tập bình thường
const renderExercisesNormal = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 2) {
            $("#exercises-challenge").append(`<div class="card normal">
        <div class="card-content w-50">
          <h4 class="card-title grey-text text-darken-4">
            ${exercisesObj[x].name_challenge}
          </h4>
          <p class="green-text darken-4">Bình thường</p>
        </div>
        <a
          class="btn btn-large btn-hitchallenge"
          href="/challenge?id=${exercisesObj[x].ID}&name_topic=${nameTopic}&id_topic=${idTopic}"
          ><span>hit challenge</span></a
        >
      </div>`);
            hitcan.kmin.edu.vn;
        }
    }
};

// hàm render ra những bài tập Khó
const renderExercisesHard = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 3) {
            $("#exercises-challenge").append(`<div class="card hard">
        <div class="card-content w-50">
          <h4 class="card-title grey-text text-darken-4">
            ${exercisesObj[x].name_challenge}
          </h4>
          <p class="orange-text darken-4">Khó</p>
        </div>
        <a
          class="btn btn-large btn-hitchallenge"
          href="/challenge?id=${exercisesObj[x].ID}&name_topic=${nameTopic}&id_topic=${idTopic}"
          ><span>hit challenge</span></a
        >
      </div>`);
        }
    }
};

// hàm render ra những bài tập cực khó
const renderExercisesExtreme = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 4) {
            $("#exercises-challenge").append(`<div class="card extreme">
        <div class="card-content w-50">
          <h4 class="card-title grey-text text-darken-4">
            ${exercisesObj[x].name_challenge}
          </h4>
          <p class="red-text darken-4">Cực khó</p>
        </div>
        <a
          class="btn btn-large btn-hitchallenge"
          href="/challenge?id=${exercisesObj[x].ID}&name_topic=${nameTopic}&id_topic=${idTopic}"
          ><span>hit challenge</span></a
        >
      </div>`);
        }
    }
};

// hàm render ra full bài tập
const renderExercisesAll = (exercisesObj) => {
    let x;
    for (x in exercisesObj) {
        if (exercisesObj[x].level == 1) {
            $("#exercises-challenge").append(`<div class="card easy">
        <div class="card-content w-50">
          <h4 class="card-title grey-text text-darken-4">
            ${exercisesObj[x].name_challenge}
          </h4>
          <p class="cyan-text darken-4">Dễ</p>
        </div>
        <a
          class="btn btn-large btn-hitchallenge"
          href="/challenge?id=${exercisesObj[x].ID}&name_topic=${nameTopic}&id_topic=${idTopic}"
          ><span>hit challenge</span></a
        >
      </div>`);
        } else if (exercisesObj[x].level == 2) {
            $("#exercises-challenge").append(`<div class="card normal">
        <div class="card-content w-50">
          <h4 class="card-title grey-text text-darken-4">
            ${exercisesObj[x].name_challenge}
          </h4>
          <p class="green-text darken-4">Bình thường</p>
        </div>
        <a
          class="btn btn-large btn-hitchallenge"
          href="/challenge?id=${exercisesObj[x].ID}&name_topic=${nameTopic}&id_topic=${idTopic}"
          ><span>hit challenge</span></a
        >
      </div>`);
        } else if (exercisesObj[x].level == 3) {
            $("#exercises-challenge").append(`<div class="card hard">
        <div class="card-content w-50">
          <h4 class="card-title grey-text text-darken-4">
            ${exercisesObj[x].name_challenge}
          </h4>
          <p class="orange-text darken-4">Khó</p>
        </div>
        <a
          class="btn btn-large btn-hitchallenge"
          href="/challenge?id=${exercisesObj[x].ID}&name_topic=${nameTopic}&id_topic=${idTopic}"
          ><span>hit challenge</span></a
        >
      </div>`);
        } else if (exercisesObj[x].level == 4) {
            $("#exercises-challenge").append(`<div class="card extreme">
        <div class="card-content w-50">
          <h4 class="card-title grey-text text-darken-4">
            ${exercisesObj[x].name_challenge}
          </h4>
          <p class="red-text darken-4">Cực khó</p>
        </div>
        <a
          class="btn btn-large btn-hitchallenge"
          href="/challenge?id=${exercisesObj[x].ID}&name_topic=${nameTopic}&id_topic=${idTopic}"
          ><span>hit challenge</span></a
        >
      </div>`);
        }
    }
};

async function getData(loadTime) {
    let data;
    let idTopic = $("#index-now").attr("id-topic");
    await axios({
        method: "get",
        url: `http://hit.kmin.edu.vn/challenges/api/get_topic/${idTopic}/${loadTime}`,
    })
        .then((res) => {
            nameTopic = res.data.data.name_topic;
            data = res.data.data.challenges_of_topic;
            endLoad = res.data.max_time_load;
        })
        .catch((err) => {
            console.log(err);
        });
    return data;
}

$(document).ready(() => {
    $(".modal").modal();

    let count = 0;
    let challenges = [];
    getData(countTimeLoad).then((res) => {
        challenges.push(res);
        countTimeLoad++;
        idTopic = $("#index-now").attr("id-topic");

        for (value of challenges) {
            renderExercisesAll(value);
        }
        Scroll(challenges);
        $("#easy").on("change", () => {
            if ($("#easy").is(":checked")) {
                $("#easy-mobile").attr("checked", true);
                if (count == 0) {
                    $("#exercises-challenge").empty();
                }
                for (value of challenges) {
                    renderExercisesEasy(value);
                }
                count++;
            } else {
                $("#easy-mobile").attr("checked", false);
                $(".easy").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#easy-mobile").on("change", () => {
            if ($("#easy-mobile").is(":checked")) {
                $("#easy").attr("checked", true);
                if (count == 0) {
                    $("#exercises-challenge").empty();
                }
                for (value of challenges) {
                    renderExercisesEasy(value);
                }
                count++;
            } else {
                $("#easy").attr("checked", false);
                $(".easy").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#normal").on("change", () => {
            if ($("#normal").is(":checked")) {
                $("#normal-mobile").attr("checked", true);
                if (count == 0) {
                    $("#exercises-challenge").empty();
                }
                for (value of challenges) {
                    renderExercisesNormal(value);
                }
                count++;
            } else {
                $("#normal-mobile").attr("checked", false);
                $(".normal").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#normal-mobile").on("change", () => {
            if ($("#normal-mobile").is(":checked")) {
                $("#normal").attr("checked", true);
                if (count == 0) {
                    $("#exercises-challenge").empty();
                }
                for (value of challenges) {
                    renderExercisesNormal(value);
                }
                count++;
            } else {
                $("#normal").attr("checked", false);
                $(".normal").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#hard").on("change", () => {
            if ($("#hard").is(":checked")) {
                $("#hard-mobile").attr("checked", true);
                if (count == 0) {
                    $("#exercises-challenge").empty();
                }
                for (value of challenges) {
                    renderExercisesHard(value);
                }
                count++;
            } else {
                $("#hard-mobile").attr("checked", false);
                $(".hard").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#hard-mobile").on("change", () => {
            if ($("#hard-mobile").is(":checked")) {
                $("#hard").attr("checked", true);
                if (count == 0) {
                    $("#exercises-challenge").empty();
                }
                for (value of challenges) {
                    renderExercisesHard(value);
                }
                count++;
            } else {
                $("#hard").attr("checked", false);
                $(".hard").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#extreme").on("change", () => {
            if ($("#extreme").is(":checked")) {
                $("#extreme-mobile").attr("checked", true);
                if (count == 0) {
                    $("#exercises-challenge").empty();
                }
                for (value of challenges) {
                    renderExercisesExtreme(value);
                }
                count++;
            } else {
                $("#extreme-mobile").attr("checked", false);
                $(".extreme").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });

        $("#extreme-mobile").on("change", () => {
            if ($("#extreme-mobile").is(":checked")) {
                $("#extreme").attr("checked", true);
                if (count == 0) {
                    $("#exercises-challenge").empty();
                }
                for (value of challenges) {
                    renderExercisesExtreme(value);
                }
                count++;
            } else {
                $("#extreme").attr("checked", false);
                $(".extreme").remove();
                count--;
            }
            if (count == 0) {
                for (value of challenges) {
                    renderExercisesAll(value);
                }
            }
        });
    });
});

function Scroll(challenges) {
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 100 && countTimeLoad <= endLoad) {
            if (loadOneTime == 0) {
                $("#loading").append(load);
                loadOneTime = 1;

                getData(countTimeLoad).then((res) => {
                    challenges.push(res);
                    if (
                        $("#easy").is(":checked") == false &&
                        $("#easy-mobile").is(":checked") == false &&
                        $("#normal").is(":checked") == false &&
                        $("#normal-mobile").is(":checked") == false &&
                        $("#hard").is(":checked") == false &&
                        $("#hard-mobile").is(":checked") == false &&
                        $("#extreme").is(":checked") == false &&
                        $("#extreme-mobile").is(":checked") == false
                    ) {
                        renderExercisesAll(res);
                    } else {
                        if ($("#easy").is(":checked") || $("#easy-mobile").is(":checked")) {
                            renderExercisesEasy(res);
                        }
                        if ($("#normal").is(":checked") || $("#normal-mobile").is(":checked")) {
                            renderExercisesNormal(res);
                        }
                        if ($("#hard").is(":checked") || $("#hard-mobile").is(":checked")) {
                            renderExercisesHard(res);
                        }
                        if ($("#extreme").is(":checked") || $("#extreme-mobile").is(":checked")) {
                            renderExercisesExtreme(res);
                        }
                    }

                    $(".preloader-wrapper").remove();
                    countTimeLoad++;
                    loadOneTime = 0;
                });
            }
        }
    });
}
