var load = `<span class="spinner-border text-warning spinner-border-sm" role="status" aria-hidden="true"></span>
Loading...`;
var flagTopic = false;

var editorProblemNeedToBeSokve = new FroalaEditor("#problem-need-to-be-solve", {
    imageUploadURL: "/problem/api/upload_image",

    imageUploadParams: {
        id: "my_editor",
    },
});
var editorDescription = new FroalaEditor("#description", {
    imageUploadURL: "/problem/api/upload_image",

    imageUploadParams: {
        id: "my_editor",
    },
});
var editorInputFormat = new FroalaEditor("#input-format", {
    imageUploadURL: "/problem/api/upload_image",

    imageUploadParams: {
        id: "my_editor",
    },
});
var editorOutputFormat = new FroalaEditor("#output-format", {
    imageUploadURL: "/problem/api/upload_image",

    imageUploadParams: {
        id: "my_editor",
    },
});
var editorConstraint = new FroalaEditor("#constraint", {
    imageUploadURL: "/problem/api/upload_image",

    imageUploadParams: {
        id: "my_editor",
    },
});

(function ($) {
    "use strict";

    const header = getCookie("Authorization");
    axios({
        headers: {
            Authorization: header,
        },
        method: "get",
        url: "http://hit.kmin.edu.vn/topics/api/get_all_topics",
    })
        .then((res) => {
            var topics = [];
            var data = res.data.data;
            for (var val of data) {
                var detail_topic = {
                    id: val.ID,
                    text: val.name_topic,
                };
                topics.push(detail_topic);
            }
            $("#topics").each(function () {
                $(this).select2({
                    placeholder: "Chủ đề",
                    data: topics,
                    tags: true,
                });
            });
        })
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
})(jQuery);

(function ($) {
    ("use strict");

    $("#challenges").each(function () {
        $(this).select2({
            placeholder: "Chọn một chủ đề!",
            tags: true,
        });
    });
})(jQuery);

(function ($) {
    "use strict";

    $("#problems-relationship").each(function () {
        $(this).select2({
            placeholder: "Chọn một challenge!",
        });
    });
})(jQuery);

GetAllChallenge();

function GetAllChallenge() {
    const header = getCookie("Authorization");
    axios({
        headers: {
            Authorization: header,
        },
        method: "get",
        url: "http://hit.kmin.edu.vn/challenge/api/get_all_challenges",
    })
        .then((res) => {
            var data = res.data.data.challenges;
            var render = [];
            for (var val of data) {
                var detail = {
                    id: val.ID,
                    text: val.name_challenge,
                };
                render.push(detail);
            }

            $("#challenge-relationship").each(function () {
                $(this).select2({
                    placeholder: "challenges",
                    data: render,
                    tags: true,
                });
            });
        })
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
}

(function ($) {
    // USE STRICT
    "use strict";
    $(".animsition").animsition({
        inClass: "fade-in",
        outClass: "fade-out",
        inDuration: 900,
        outDuration: 900,
        linkElement: 'a:not([target="_blank"]):not([href^="#"]):not([class^="chosen-single"])',
        loading: true,
        loadingParentElement: "html",
        loadingClass: "page-loader",
        loadingInner: '<div class="page-loader__spin"></div>',
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: ["animation-duration", "-webkit-animation-duration"],
        overlay: false,
        overlayClass: "animsition-overlay-slide",
        overlayParentElement: "html",
        transition: function (url) {
            window.location.href = url;
        },
    });
})(jQuery);

$(document).ready(function () {
    $("#topics").on("select2:select", function (e) {
        var reg = new RegExp("^\\d+$");
        var data = e.params.data.id;
        nameTopicChoose = data;
        flagTopic = true;
        if (reg.test(data)) {
            $("#challenges").empty().trigger("change");
            GetChallengeByTopic(data);
        } else {
            $("#challenges").empty().trigger("change");
        }
    });

    $("#challenges").on("select2:select", function (e) {
        var reg = new RegExp("^\\d+$");
        var data = e.params.data.id;
        if (flagTopic === true) {
            flagTopic = true;
        } else {
            flagTopic = false;
        }
    });

    $("#challenge-relationship").on("select2:select", function (e) {
        var data = e.params.data.id;
        GetProblemOfChallenge(data);
    });

    $("#add-input-output").on("click", function () {
        var count = $(this).attr("count");
        if ($(`#sample-input-${count}`).val() != "" || $(`#sample-output-${count}`).val() != "") {
            count++;
            $(this).attr("count", count);

            var inputDom = `
            <div class="col col-md-3 mt-3">
                <label for="sample-input-${count}" class=" form-control-label">Đầu vào mẫu</label>
            </div>
            <div class="col-12 col-md-9 mt-3">
                <textarea
                    type="text"
                    id="sample-input-${count}"
                    name="sample-input-${count}"
                    placeholder="Đầu vào mẫu bài toán"
                    class="form-control input-exemple"
                    value=""
                    rows="5"></textarea>
            </div>
            <div class="col col-md-3 mt-3">
                <label for="sample-output-${count}" class=" form-control-label">Đầu ra mẫu</label>
            </div>
            <div class="col-12 col-md-9 mt-3">
                <textarea
                    type="text"
                    id="sample-output-${count}"
                    name="sample-output-${count}"
                    placeholder="Đầu ra mẫu bài toán"
                    class="form-control output-exemple"
                    value=""
                    rows="5"></textarea>
            </div>`;

            $("#new-input-output").append(inputDom);
        }
    });

    $("#add-youtube").on("click", function () {
        var count = $(this).attr("count");
        if ($("#link-youtube-1").val() != "") {
            count++;
            $(this).attr("count", count);

            var inputDom = `
            <div class="col col-md-3 mt-3">
                <label for="link-youtube-${count}" class=" form-control-label">Link Youtube</label>
            </div>
            <div class="col-12 col-md-9 mt-3">
                <textarea
                    type="text"
                    id="link-youtube-${count}"
                    name="link-youtube-${count}"
                    placeholder="Lấy toàn bộ iframe của youtube"
                    value=""
                    class="form-control youtube-link"></textarea>
            </div>`;
            $("#add-new-link-youtube").append(inputDom);
        }
    });

    $("#submit-new-problem").on("click", function () {
        $(this).html("").append(load);
        submitNewProblem();
    });
});

function GetChallengeByTopic(id) {
    const header = getCookie("Authorization");
    axios({
        headers: {
            Authorization: header,
        },
        method: "get",
        url: "http://hit.kmin.edu.vn/challenge/api/get_challenge_by_topic?id_topic=" + id,
    })
        .then((res) => {
            var data = res.data.data.challenges;
            var render = [];
            for (var val of data) {
                var detail = {
                    id: val.ID,
                    text: val.name_challenge,
                };
                render.push(detail);
            }
            $("#challenges").each(function () {
                $(this).select2({
                    placeholder: "Challenges",
                    data: render,
                    tags: true,
                });
            });
        })
        .catch((err) => {
            $("#noti-content").html(
                err.response.data ? err.response.data.error.message : "Không xhttp://hitcan.kminử lý được yêu cầu"
            );
            $("#notification").toast("show");
        });
}

function GetProblemOfChallenge(id) {
    const header = getCookie("Authorization");
    const requestChallenge = axios.get("http://hit.kmin.edu.vn/challenge/api/get_challenge/" + id, {
        headers: { Authorization: header },
    });
    const requestAllProblem = axios.get("http://hit.kmin.edu.vn/problem/api/get_all_problems", {
        headers: { Authorization: header },
    });
    const requestProblemOfChallenge = axios.get(
        "http://hit.kmin.edu.vn/problem/api/get_problems_by_challenge?id_challenge=" + id,
        { headers: { Authorization: header } }
    );

    axios
        .all([requestAllProblem, requestProblemOfChallenge, requestChallenge])
        .then(
            axios.spread((res1, res2, res3) => {
                allProblem = res1.data.data.problems;
                problemsOfChallenge = res2.data.data.problems;
                $("#problems-relationship").empty().trigger("change");

                $("#level-of-challenge").empty().trigger("change");

                const dataMap = allProblem.map((allPro) => {
                    const selectedID = problemsOfChallenge.find((problem) => problem.ID == allPro.ID);
                    return {
                        id: allPro.ID,
                        text: allPro.name_problem,
                        selected: selectedID,
                    };
                });

                $("#problems-relationship").each(function () {
                    $(this).select2({
                        data: dataMap,
                        tags: true,
                    });
                });

                var challenge = res3.data.data.challenge;
                var dataLevel = [];
                for (var i = 1; i <= 4; i++) {
                    var nameLevel;
                    switch (i) {
                        case 1:
                            nameLevel = "Dễ";
                            break;
                        case 2:
                            nameLevel = "Bình thường";
                            break;
                        case 3:
                            nameLevel = "Khó";
                            break;
                        case 4:
                            nameLevel = "Cực khó";
                            break;
                    }
                    if (challenge.level == i) {
                        var level = {
                            id: i,
                            text: nameLevel,
                            selected: i,
                        };
                        dataLevel.push(level);
                    } else {
                        var level = {
                            id: i,
                            text: nameLevel,
                        };
                        dataLevel.push(level);
                    }
                }
                $("#level-of-challenge").each(function () {
                    $(this).select2({
                        data: dataLevel,
                    });
                });
            })
        )
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
}

function SubmitChallengeHasProblem() {
    var IDdataChallenge = $("#challenge-relationship").val();
    var IDdataProblem = $("#problems-relationship").val();
    var nameChallenge = $("#new-challenge-name").val();
    var level = $("#level-of-challenge").val();

    $("#submit-challenge-problem").html("").append(load);

    var problemOfChallenge = IDdataProblem.map((ID) => {
        return {
            id: parseInt(ID),
        };
    });

    nameChallenge = nameChallenge.trim();
    var dataRequest = {};
    if (nameChallenge !== "") {
        dataRequest = {
            id: parseInt(IDdataChallenge),
            name_challenge: nameChallenge,
            problems_of_challenge: problemOfChallenge,
            level: parseInt(level),
        };
    } else {
        dataRequest = {
            id: parseInt(IDdataChallenge),
            problems_of_challenge: problemOfChallenge,
            level: parseInt(level),
        };
    }

    const header = getCookie("Authorization");
    axios({
        method: "put",
        headers: {
            Authorization: header,
        },
        url: "http://hit.kmin.edu.vn/challenge/api/update_challenge",
        data: dataRequest,
    })
        .then((res) => {
            $("#submit-challenge-problem").html("").text("Submit");
        })
        .catch((err) => {
            $("#submit-challenge-problem").html("").text("Submit");
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
}

function submitNewProblem() {
    var dataRequest = getDataProblem();
    if (dataRequest !== undefined) {
        const header = getCookie("Authorization");
        axios({
            method: "put",
            headers: {
                Authorization: header,
            },
            url: "http://hit.kmin.edu.vn/problem/api/udpate_problem",
            data: dataRequest,
        })
            .then(() => {
                $("#submit-new-problem").html("").text("Submit");
                location.reload();
            })
            .catch((err) => {
                $("#submit-new-problem").html("").text("Submit");
                $("#noti-content").html(
                    err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                );
                $("#notification").toast("show");
            });
    } else {
        $("#submit-new-problem").html("").text("Submit");
        $("#noti-content").html("Bạn cần nhập đủ thông tin!");
        $("#notification").toast("show");
    }
}

function getDataProblem() {
    var id = $("#name-problem").attr("id-problem");
    var name = $("#name-problem").val();
    var premium = $("#premium").val();
    var level = $("#level").val();
    var problemNeedToBeSolve = editorProblemNeedToBeSokve.html.get();
    var description = editorDescription.html.get();
    var inputFormat = editorInputFormat.html.get();
    var outputFormat = editorOutputFormat.html.get();
    var constraint = editorConstraint.html.get();
    var inputExemple = $(".input-exemple").each(function () {
        return $(this).val().value;
    });
    var outputExemple = $(".output-exemple").each(function () {
        return $(this).val().value;
    });
    var link = $(".youtube-link").each(function () {
        return $(this).val().value;
    });
    name = name.trim();
    problemNeedToBeSolve = removeStringFroala(problemNeedToBeSolve);
    description = removeStringFroala(description);
    inputFormat = removeStringFroala(inputFormat);
    outputFormat = removeStringFroala(outputFormat);
    constraint = removeStringFroala(constraint);
    description = description.trim();
    inputFormat = inputFormat.trim();
    outputFormat = outputFormat.trim();
    constraint = constraint.trim();

    if (name != "" && premium != undefined && level != undefined && flagTopic == true && link.length >= 1) {
        var exemple = [];
        for (var i = 0; i < inputExemple.length; i++) {
            if (inputExemple[i].value != "" || outputExemple[i].value != "") {
                var dataExemple = {
                    sample_input: inputExemple[i].value,
                    sample_output: outputExemple[i].value,
                };
                exemple.push(dataExemple);
            }
        }
        var linkYoutube = [];
        for (var i = 0; i < link.length; i++) {
            if (link[i].value != "") {
                var dataLink = {
                    youtube_link: link[i].value,
                };
                linkYoutube.push(dataLink);
            }
        }

        if (linkYoutube.length < 1) {
            return undefined;
        }
        var reg = new RegExp("^\\d+$");
        var nameTopicChoose = $("#topics").val();
        var nameChallengeChoose = $("#challenges").val();
        var topic;
        if (topic != "") {
            if (reg.test(nameTopicChoose)) {
                topic = {
                    ID: parseInt(nameTopicChoose),
                };
            } else {
                topic = {
                    name_topic: nameTopicChoose,
                };
            }
        }
        var challenges = nameChallengeChoose.map((data) => {
            if (reg.test(data)) {
                return {
                    ID: parseInt(data),
                };
            } else {
                return {
                    name_challenge: data,
                };
            }
        });

        var dataProblem = {
            ID: parseInt(id),
            name_problem: name,
            premium: parseInt(premium),
            challenges_of_problem: challenges,
            level: parseInt(level),
            problem_need_to_be_solve: problemNeedToBeSolve,
            description: description,
            constraint: constraint,
            input_format: inputFormat,
            output_format: outputFormat,
            inputs_outputs_for_problem: exemple,
            youtube_links: linkYoutube,
        };
        return {
            topic: topic,
            challenges: challenges,
            problem: dataProblem,
        };
    }
}

function removeStringFroala(str) {
    return str.replace(
        `<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>`,
        ""
    );
}
