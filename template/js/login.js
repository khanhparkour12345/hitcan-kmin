let idCode;
let flag = false;
let emailRegex = /^\S+@\S+$/g;
let mediumRegex = new RegExp("((?=.*[a-z])(?=.*[0-9])(?=.{6,}))");
let strongPassword = new RegExp("((?=.*[a-z])(?=.*[0-9])(?=.{8,}))");
let continue_submit = false;

$(document).ready(function () {
    $("#email").on("input", () => {
        let emailValue = $("#email").val();
        if (emailRegex.test(emailValue)) {
            continue_submit = true;
        } else {
            continue_submit = false;
        }
    });

    $("#google-login").on("click", () => {
        window.location = "/auth/google/login";
    });

    $("#facebook-login").on("click", () => {
        window.location = "https://kmin.edu.vn/auth/facebook/login";
    });
});

$("#new-password").on("input", function () {
    let inputPass = $(this).val();
    if (strongPassword.exec(inputPass)) {
        $("#new-password").addClass("valid");
        $("#new-password").removeClass("invalid");
        $("#helper-password").removeClass("medium");
        flag = true;
    } else if (mediumRegex.exec(inputPass)) {
        $("#new-password").addClass("invalid");
        $("#helper-password").addClass("medium");
        $("#helper-password").attr("data-error", "medium password");
        flag = true;
    } else {
        $("#new-password").removeClass("validate");
        $("#new-password").addClass("invalid");
        $("#helper-password").removeClass("medium");
        $("#helper-password").attr("data-error", "weak password");
        flag = false;
    }
});

const login = () => {
    const data = {
        email: $("#email").val(),
        password: $("#password").val(),
    };

    axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/info_login",
        data: data,
    })
        .then((res) => {
            let premium = res.data.data.premium;
            if (premium < 2) {
                let d = new Date();
                d.setTime(d.getTime() + 12500 * 24 * 60 * 60 * 1000);
                document.cookie = `Authorization=${res.data.token}; expires=${d.toGMTString()}; path=/`;
                window.location = "/course";
            } else {
                let d = new Date();
                d.setTime(d.getTime() + 12500 * 24 * 60 * 60 * 1000);
                document.cookie = `Authorization=${res.data.token}; expires=${d.toGMTString()}; path=/`;
                window.location = "/admin_index";
            }
        })
        .catch((err) => {
            $(".preloader-wrapper").remove();
            $("#login").html("Đăng nhập");
            let toastHTML = `<i class="material-icons">
          sentiment_very_dissatisfied
          </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${
              err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
          }</span></span>`;
            M.toast({ html: toastHTML });
        });
};

async function checkEmailExist(email) {
    const flag = await axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/check_email_exist",
        data: {
            email: email,
        },
    })
        .then((res) => {
            $(".preloader-wrapper").remove();
            $("#get-email").html("Nhận ID Code");
            let toastHTML = `<i class="material-icons">
          sentiment_very_dissatisfied
          </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">Tài khoản chưa được tạo</span></span>`;
            M.toast({ html: toastHTML });
            return false;
        })
        .catch((err) => {
            return true;
        });

    return flag;
}

async function sendMail(email, buttonLoading, titleButton) {
    let code;
    await axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/send_email",
        data: {
            email: email,
        },
    })
        .then((res) => {
            code = res.data.confirm_email;
            $("#get-email-tab").addClass("d-none").removeClass("d-block");
            $("#get-id-code").addClass("d-block").removeClass("d-none");
            $(".preloader-wrapper").remove();
            $(`#${buttonLoading}`).html(titleButton);
        })
        .catch((err) => {
            $(".preloader-wrapper").remove();
            $(`#${buttonLoading}`).html(titleButton);
            let toastHTML = `<i class="material-icons">
      sentiment_very_dissatisfied
      </i><br><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darkend-3">Không thể gửi email!</span>
      </span>`;
            M.toast({ html: toastHTML });
            code = "";
        });
    return code;
}

const updatePassword = (email, password) => {
    axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/update_password",
        data: {
            email: email,
            password: password,
        },
    })
        .then(() => {
            $(".preloader-wrapper").remove();
            $("#submit-pass").html("Lưu");
            $("#forget-password").modal("close");
            success();
        })
        .catch((err) => {
            $(".preloader-wrapper").remove();
            $("#submit-pass").html("Lưu");
            let toastHTML = `<i class="material-icons">
          sentiment_very_dissatisfied
          </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${
              err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
          }</span></span>`;
            M.toast({ html: toastHTML });
        });
};

function loginSubmit() {
    if ($("#email").val() !== "" && $("#password").val() !== "") {
        $("#login").html("").append(`
    <div class="preloader-wrapper small active">
      <div class="spinner-layer spinner-yellow-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div>
        <div class="gap-patch">
          <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
    `);
        login();
    }
}

function submitPass() {
    if (flag == true) {
        $("#submit-pass").html("").append(`
    <div class="preloader-wrapper small active">
      <div class="spinner-layer spinner-yellow-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div>
        <div class="gap-patch">
          <div class="circle"></div>
        </div>
        <div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
    `);
        updatePassword($("#email-forget-pass").val(), $("#new-password").val());
    }
}

function submitIDCode() {
    let codeInput = $("#id-code").val();
    codeInput = codeInput.trim();
    if (codeInput === idCode) {
        $("#get-id-code").addClass("d-none").removeClass("d-block");
        $("#submit-new-pass").addClass("d-block").removeClass("d-none");
    } else {
        let toastHTML = `<i class="material-icons">
        sentiment_very_dissatisfied
        </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text dartken-3"></span>Không khớp!
        </span>`;
        M.toast({ html: toastHTML });
    }
}

function getEmail() {
    if ($("#email-forget-pass").val() !== "") {
        $("#get-email").html("").append(`
      <div class="preloader-wrapper small active">
        <div class="spinner-layer spinner-yellow-only">
          <div class="circle-clipper left">
              <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
      `);
        checkEmailExist($("#email-forget-pass").val()).then((result) => {
            if (result == true) {
                sendMail($("#email-forget-pass").val(), "get-email", "Nhận ID Code").then((code) => {
                    idCode = code;
                });
            }
        });
    }
}

function sendEmailAgain() {
    $("#send-mail-again").html("").append(`
  <div class="preloader-wrapper small active">
    <div class="spinner-layer spinner-yellow-only">
      <div class="circle-clipper left">
          <div class="circle"></div>
      </div>
      <div class="gap-patch">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>
  `);
    sendMail($("#email-forget-pass").val(), "send-mail-again", "Gửi lại").then((code) => {
        idCode = code;
    });
}

function getBack() {
    $("#get-email-tab").addClass("d-block").removeClass("d-none");
    $("#get-id-code").addClass("d-none").removeClass("d-block");
}
