$(document).ready(function() {
    $(".modal").modal();
    convertHTML();

    $(".owl-carousel").owlCarousel({
        loop: false,
        margin: 10,
        items: 1,
        nav: true,
        navText: [
            '<button class="btn btn-small waves-effect waves-light yellow darken-1 m-3 black-text"><i class="material-icons">chevron_left</i></button>',
            '<button class="btn btn-small waves-effect waves-light yellow darken-1 m-3 black-text"><i class="material-icons">chevron_right</i></button>'
        ],
        dots: false
    });

    $("#need-help").on("click", () => {
        if (confirm("Hãy chắc rằng bạn đã hết cách!")) {
            $("#modal1").modal("open");
        }
    });

    $(".owl-nav").addClass("center");

    $("#submit-feedback").on("click", function() {
        let header = getCookie("Authorization");
        let idProblem = $(this).attr("id-problem");
        let radioValue = $("input[name='understanding']:checked").val();
        let comment = $("#comment").val();
        $(this).html("");
        addLoadingToHTML();

        axios({
            headers: {
                Authorization: header
            },
            method: "post",
            url: `/api/form_feedback/${idProblem}`,
            data: {
                understanding: Number(radioValue),
                comment: comment
            }
        })
            .then(res => {
                success();
                $(this).html("Gửi phản hồi");
                removeLoadingToHTML();
                $("#comment").val("");
            })
            .catch(err => {
                $(this).html("Gửi phản hồi");
                removeLoadingToHTML();
                let toastHTML = `<i class="material-icons">
                  sentiment_very_dissatisfied
                  </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"}</span></span>`;
                M.toast({ html: toastHTML });
            });
    });
});

const convertHTML = () => {
    let listVideo = $(".video-container");
    for (let i = 0; i < listVideo.length; i++) {
        let str = listVideo[i].textContent;
        listVideo[i].textContent = "";
        html = $.parseHTML(str);
        listVideo[i].innerHTML = str;
    }
};

const addLoadingToHTML = () => {
    $("#submit-feedback").append(`
  <div class="preloader-wrapper small active">
    <div class="spinner-layer spinner-yellow-only">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div>
      <div class="gap-patch">
        <div class="circle"></div>
      </div>
      <div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>`);
};

const removeLoadingToHTML = () => {
    $(".preloader-wrapper").remove();
};
