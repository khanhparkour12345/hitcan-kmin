(function ($) {
    // USE STRICT
    "use strict";
    $('[data-toggle="tooltip"]').tooltip();

    try {
        //WidgetChart 1
        const header = getCookie("Authorization");
        axios({
            headers: {
                Authorization: header,
            },
            method: "get",
            url: "http://hit.kmin.edu.vn/account/api/get_number_of_accounts",
        })
            .then((res) => {
                var data = res.data.number_of_accounts;
                $("#number-accounts").html(data);
            })
            .catch((err) => {
                $("#noti-content").html(
                    err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                );
                $("#notification").toast("show");
            });

        //WidgetChart 2
        axios({
            headers: {
                Authorization: header,
            },
            method: "get",
            url: "http://hit.kmin.edu.vn/feedback/api/get_number_of_feedbacks",
        })
            .then((res) => {
                var data = res.data.number_of_feedbacks;
                $("#number-feedbacks").html(data);
            })
            .catch((err) => {
                $("#noti-content").html(
                    err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                );
                $("#notification").toast("show");
            });

        axios({
            headers: {
                Authorization: header,
            },
            method: "get",
            url: "http://hit.kmin.edu.vn/problem/api/get_number_of_porblems",
        })
            .then((res) => {
                var data = res.data.number_of_problems;
                $("#number-exercises").html(data);
            })
            .catch((err) => {
                $("#noti-content").html(
                    err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                );
                $("#notification").toast("show");
            });

        //WidgetChart 4
        axios({
            headers: {
                Authorization: header,
            },
            method: "get",
            url: "http://hit.kmin.edu.vn/feedback/api/get_detail_chart",
        })
            .then((res) => {
                var data = res.data.data;
                var numberDontUnderstand = data.number_dont_understand;
                var numberNormal = data.number_normal;
                var numberUnderstand = data.number_understand;
                var ctx = document.getElementById("widgetChart4");
                if (ctx) {
                    ctx.height = 115;
                    var myChart = new Chart(ctx, {
                        type: "bar",
                        data: {
                            labels: ["Chưa hiểu", "Tạm hiểu", "Rất hiểu"],
                            datasets: [
                                {
                                    label: "số lượt feedback",
                                    data: [numberDontUnderstand, numberNormal, numberUnderstand, 0],
                                    borderColor: "transparent",
                                    borderWidth: "0",
                                    backgroundColor: "rgba(255,255,255,.3)",
                                },
                            ],
                        },
                        options: {
                            maintainAspectRatio: true,
                            legend: {
                                display: false,
                            },
                            scales: {
                                xAxes: [
                                    {
                                        display: false,
                                        // categoryPercentage: 1,
                                        // barPercentage: 0.65,
                                    },
                                ],
                                yAxes: [
                                    {
                                        display: false,
                                    },
                                ],
                            },
                        },
                    });
                }
            })
            .catch((err) => {
                $("#noti-content").html(
                    err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
                );
                $("#notification").toast("show");
            });

        // Recent Report
        const brandProduct = "rgba(0,181,233,0.8)";
        const brandService = "rgba(0,173,95,0.8)";
    } catch (err) {
        $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
        $("#notification").toast("show");
    }
})(jQuery);

(function ($) {
    // USE STRICT
    "use strict";
    $(".animsition").animsition({
        inClass: "fade-in",
        outClass: "fade-out",
        inDuration: 900,
        outDuration: 900,
        linkElement: 'a:not([target="_blank"]):not([href^="#"]):not([class^="chosen-single"])',
        loading: true,
        loadingParentElement: "html",
        loadingClass: "page-loader",
        loadingInner: '<div class="page-loader__spin"></div>',
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: ["animation-duration", "-webkit-animation-duration"],
        overlay: false,
        overlayClass: "animsition-overlay-slide",
        overlayParentElement: "html",
        transition: function (url) {
            window.location.href = url;
        },
    });
})(jQuery);

(function ($) {
    // USE STRICT
    "use strict";

    // Scroll Bar
    try {
        var jscr1 = $(".js-scrollbar1");
        if (jscr1[0]) {
            const ps1 = new PerfectScrollbar(".js-scrollbar1");
        }

        var jscr2 = $(".js-scrollbar2");
        if (jscr2[0]) {
            const ps2 = new PerfectScrollbar(".js-scrollbar2");
        }
    } catch (error) {
        console.log(error);
    }
})(jQuery);

//-----------------------------------------------------------------------------------------------------------------------
(function ($) {
    // USE STRICT
    "use strict";

    // Select 2
    try {
        $(".js-select2").each(function () {
            $(this).select2({
                minimumResultsForSearch: 100,
                dropdownParent: $(this).next(".dropDownSelect2"),
                selectOnClose: true,
                closeOnSelect: false,
            });
        });
        $("#member").each(function () {
            $(this).select2({
                minimumResultsForSearch: 300,
                dropdownParent: $(this).next(".dropDownSelect2"),
                dropdownParent: ".modal",
                selectOnClose: true,
                closeOnSelect: false,
            });
        });
    } catch (error) {
        console.log(error);
    }
})(jQuery);
