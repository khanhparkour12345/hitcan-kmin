const header = getCookie('Authorization');
var users = [];
var waiting = false;

$(document).ready(function () {
    $('#add-new-user').on('click', function () {
        var names = $('.name');
        var emails = $('.email');
        var passwords = $('.password');
        var lastName = names[names.length - 1];
        var lastEmail = emails[emails.length - 1];
        var lastPassword = passwords[passwords.length - 1];
        if (
            lastName.value !== '' &&
            verifiedEmail(lastEmail.value) &&
            lastPassword.value.length > 8
        ) {
            var user = {
                email: lastEmail.value,
                name: lastName.value,
                password: lastPassword.value,
            };
            users.push(user);
            $('.row-input').append(`
                <div class="row">
                    <div class="col-4">
                        <input type="text" class="form-control name" value="" placeholder="Name">
                    </div>
                    <div class="col-4">
                        <input type="text" class="form-control email" value="" placeholder="Email">
                    </div>
                    <div class="col-4">
                        <input type="password" class="form-control password" value="" placeholder="Password">
                    </div>
                </div>
            `);
        }
        console.log(users);
    });

    $('#save').click(function () {
        if (waiting == false) {
            waiting = true;
            var names = $('.name');
            var emails = $('.email');
            var passwords = $('.password');
            var lastName = names[names.length - 1];
            var lastEmail = emails[emails.length - 1];
            var lastPassword = passwords[passwords.length - 1];
            var courseID = $('.id_course')[0].value;
            if (
                lastName.value !== '' &&
                verifiedEmail(lastEmail.value) &&
                lastPassword.value.length > 8
            ) {
                var user = {
                    email: lastEmail.value,
                    name: lastName.value,
                    password: lastPassword.value,
                };
                users.push(user);
            }
            var dataReq = {
                course_id: parseInt(courseID),
                users: users,
            };
            if (users.length > 0) {
                postUser(dataReq);
            }
        }
    });
});

function verifiedEmail(email) {
    if (
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
            email
        )
    ) {
        return true;
    }
    return false;
}

async function postUser(usersData) {
    await axios({
        method: 'post',
        headers: {
            Authorization: header,
        },
        url: 'http://hit.kmin.edu.vn/account/api/add_user',
        data: usersData,
    })
        .then(function (res) {
            window.location.reload(true);
        })
        .catch(function (err) {
            return err;
        });
}
