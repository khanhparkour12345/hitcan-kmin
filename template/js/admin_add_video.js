const header = getCookie("Authorization");
var videos = [];
var waiting = false;

$(document).ready(function () {
  var chapterID = $(".id_chapter").attr("chapter_id");
  $(".id_course").change(function () {
    var idCouse = $(this).val();
    console.log(idCouse);
    chapterID = getChapter(idCouse).then(function (data) {
      $(".id_chapter").empty();
      var count = 0;
      for (var element of data) {
        $(".id_chapter").append(
          `<option value="${element.ID}">${element.name}</option>`
        );
        if (count == 0) {
          chapterID = element.ID;
          getLesson(chapterID).then(function (lessons) {
            $(".id_lesson").empty();
            for (var element of lessons) {
              $(".id_lesson").append(
                `<option value="${element.ID}">${element.stt}: ${element.name}</option>`
              );
            }
          });
          count++;
        }
      }
      return chapterID;
    });
  });

  $(".id_chapter").change(function () {
    chapterID = $(this).val();
    getLesson(chapterID).then(function (lessons) {
      $(".id_lesson").empty();
      for (var element of lessons) {
        $(".id_lesson").append(
          `<option value="${element.ID}">${element.stt}: ${element.name}</option>`
        );
      }
    });
  });

  $("#add-new-video").click(function () {
    var iframe = $(".iframe");
    var lessons = $(".id_lesson");
    var lastItem = iframe[iframe.length - 1];
    var lastLesson = lessons[lessons.length - 1];
    console.log(lastItem.value);
    if (lastItem.value !== "") {
      var dataVideo = {
        id_lesson: parseInt(lastLesson.value),
        iframe: lastItem.value,
      };
      videos.push(dataVideo);
      $(".row-input").append(`
			<div class="row">
                <div class="col-4">
                    <select class="id_lesson custom-select mr-sm-2" name="id_lesson">
					</select>
                </div>
                <div class="col-7">
                    <input class="iframe form-control"
                    type="text" value=""
                    name="iframe" placeholder="iframe video"/>
                </div>
			</div>`);

      getLesson(chapterID).then(function (lessons) {
        for (var element of lessons) {
          $(".id_lesson:last").append(
            `<option value="${element.ID}">${element.stt}: ${element.name}</option>`
          );
        }
      });
    }
    console.log(videos);
  });

  $("#save").click(function () {
    if (waiting == false) {
      waiting = true;
      var iframe = $(".iframe");
      var lessons = $(".id_lesson");
      var lastItem = iframe[iframe.length - 1];
      var lastLesson = lessons[lessons.length - 1];
      if (lastItem.value !== "") {
        var dataVideo = {
          id_lesson: parseInt(lastLesson.value),
          iframe: lastItem.value,
        };
        videos.push(dataVideo);
      }
      postVideo(videos);
    }
  });
});

async function getChapter(id) {
  var data;
  await axios
    .get(`http://hit.kmin.edu.vn/chapter/api/get_chapters_by_couse/${id}`, {
      headers: { Authorization: header },
    })
    .then(function (res) {
      data = res.data.data;
    });
  return data;
}

async function getLesson(id) {
  var data;
  await axios
    .get(`http://hit.kmin.edu.vn/lesson/api/get_lessons_by_chapter/${id}`, {
      headers: { Authorization: header },
    })
    .then(function (res) {
      data = res.data.data;
    });
  return data;
}

async function postVideo(dataSend) {
  await axios({
    method: "post",
    headers: {
      Authorization: header,
    },
    url: "http://hit.kmin.edu.vn/lesson/api/add_video",
    data: dataSend,
  })
    .then(function (res) {
      window.location.reload(true);
    })
    .catch(function (err) {
      return err;
    });
}
