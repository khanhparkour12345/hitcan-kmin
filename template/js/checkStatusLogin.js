$(document).ready(() => {
    let mediumRegex = new RegExp("(?=.{6,})");
    let strongPassword = new RegExp("(?=.{8,})");
    let phoneRegex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    let emailRegex = /^\S+@\S+$/g;

    $("#google-login").on("click", () => {
        window.location = "/auth/google/login";
    });

    $("#facebook-login").on("click", () => {
        window.location = "https://kmin.edu.vn/auth/facebook/login";
    });

    let email, password, phone, name;

    let flag,
        continue_value = false,
        continue_submit = false,
        inputPass;

    $("#email").on("input", () => {
        let emailValue = $("#email").val();
        if (emailRegex.test(emailValue)) {
            continue_submit = true;
        } else {
            continue_submit = false;
        }
    });

    $("#phone").on("input", () => {
        let phoneNumber = $("#phone").val();
        if (phoneRegex.test(phoneNumber)) {
            if (continue_submit == false) {
                continue_submit = true;
            }
            $("#phone").removeClass("validate").removeClass("invalid").addClass("valid");
        } else {
            continue_submit = false;
            $("#phone").removeClass("validate").removeClass("valid").addClass("invalid");
        }
    });

    $("#password").on("input", () => {
        if (continue_value === true) {
            continue_value = false;
            removeConfirmPass();
        }
        inputPass = $("#password").val();
        if (strongPassword.exec(inputPass)) {
            $("#password").addClass("valid");
            $("#password").removeClass("invalid");
            $("#helper-password").removeClass("medium");
            flag = true;
        } else if (mediumRegex.exec(inputPass)) {
            $("#password").addClass("invalid");
            $("#helper-password").addClass("medium");
            $("#helper-password").attr("data-error", "medium password");
            flag = true;
        } else {
            $("#password").removeClass("validate");
            $("#password").addClass("invalid");
            $("#helper-password").removeClass("medium");
            $("#helper-password").attr("data-error", "weak password");
            flag = false;
        }
    });

    $("#confirm-password").on("input", () => {
        let confirmPass = $("#confirm-password").val();
        if (flag == true && confirmPass == inputPass) {
            $("#confirm-password").removeClass("invalid");
            $("#confirm-password").addClass("valid");
            continue_value = true;
        } else {
            $("#confirm-password").addClass("invalid");
            $("#confirm-password").removeClass("validate");
            continue_value = false;
        }
    });

    const removeConfirmPass = () => {
        $("#confirm-password").addClass("invalid");
        $("#confirm-password").removeClass("validate");
    };

    $("#submit-form").on("click", () => {
        if ($("#name").val() !== "" && continue_submit === true && continue_value === true) {
            $("#submit-form").html("");
            $("#submit-form").append(`<div class="preloader-wrapper small active">
        <div class="spinner-layer spinner-yellow-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>`);
            email = $("#email").val();
            password = $("#password").val();
            phone = $("#phone").val();
            name = $("#name").val();

            checkEmailExist(email, password, name, phone).then(function (result) {
                if (result == false) {
                    axios({
                        method: "post",
                        url: "http://hit.kmin.edu.vn/account/api/send_email",
                        data: {
                            email: email,
                            password: password,
                            name: name,
                            phone: phone,
                        },
                    })
                        .then((req) => {
                            let flag = false;
                            let codeSendAgain;
                            $(".preloader-wrapper").remove();
                            $("#submit-form").html("Đăng ký");
                            $("#confirm-signup").modal("open");
                            $("#stop-click").removeClass("d-none").addClass("d-block");

                            $("#send-email-again").on("click", function () {
                                $("#send-email-again").html("").append(`
                <div class="preloader-wrapper small active">
                  <div class="spinner-layer spinner-yellow-only">
                    <div class="circle-clipper left">
                      <div class="circle"></div>
                    </div><div class="gap-patch">
                      <div class="circle"></div>
                    </div><div class="circle-clipper right">
                      <div class="circle"></div>
                    </div>
                  </div>
                  </div>
                `);
                                sendMailAgain(email, password, name, phone).then(function (result) {
                                    codeSendAgain = result;
                                    $("#send-email-again").html("Gửi lại mail");
                                    $(".preload-wrapper").remove();
                                });
                                flag = true;
                            });

                            $("#submit-id-code").on("click", function () {
                                $(this).html("").append(`
                <div class="preloader-wrapper small active">
                  <div class="spinner-layer spinner-yellow-only">
                    <div class="circle-clipper left">
                      <div class="circle"></div>
                    </div><div class="gap-patch">
                      <div class="circle"></div>
                    </div><div class="circle-clipper right">
                      <div class="circle"></div>
                    </div>
                  </div>
                  </div>
                `);
                                if (flag === false) {
                                    checkEmail(req.data.confirm_email, email, password, name, phone);
                                } else {
                                    checkEmail(codeSendAgain, email, password, name, phone);
                                }
                            });

                            $("#delete-account").on("click", function () {
                                $("#confirm-signup").modal("close");
                            });
                        })
                        .catch((err) => {
                            $(".preloader-wrapper").remove();
                            $("#submit-form").html("Đăng ký");
                            let toastHTML = `<i class="material-icons">
              sentiment_very_dissatisfied
              </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${
                  err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
              }</span></span>`;
                            M.toast({ html: toastHTML });
                        });
                }
            });
        }
    });
});

async function sendMailAgain(email, password, name, phone) {
    let code;
    await axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/send_email",
        data: {
            email: email,
            password: password,
            name: name,
            phone: phone,
        },
    })
        .then((res) => {
            code = res.data.confirm_email;
        })
        .catch((err) => {
            let toastHTML = `<i class="material-icons">
      sentiment_very_dissatisfied
      </i><br><span>&nbsp;&nbsp;&nbsp;&nbsp;${
          err.response.data ? err.response.error.message : "Không thể gửi email"
      }</span>`;
            M.toast({ html: toastHTML });
            code = "";
        });
    return code;
}

const submitCodeDone = (email, password, name, phone, status) => {
    const mydata = {
        email: email,
        password: password,
        name: name,
        phone: phone,
    };

    if (status === true) {
        axios({
            method: "post",
            url: "http://hit.kmin.edu.vn/account/api/create_account",
            data: mydata,
        })
            .then((res) => {
                window.location = "/login";
            })
            .catch((err) => {
                $("#submit-id-code").html("Xác thực");
                $(".preload-wrapper").remove();
                let toastHTML = `<i class="material-icons">
          sentiment_very_dissatisfied
          </i><span>&nbsp;&nbsp;&nbsp;&nbsp;${
              err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
          }!</span>`;
                M.toast({ html: toastHTML });
            });
    } else {
        $("#submit-id-code").html("Xác thực");
        $(".preload-wrapper").remove();
        let toastHTML = `<i class="material-icons">
          sentiment_very_dissatisfied
          </i><span>&nbsp;&nbsp;&nbsp;&nbsp;Không khớp!</span>`;
        M.toast({ html: toastHTML });
    }
};

const closeModal = () => {
    $("#stop-click").removeClass("d-block").addClass("d-none");
    $("#confirm-signup").modal("close");
};

const checkEmail = (code, email, password, name, phone) => {
    let codeInput = $("#id-code").val();
    codeInput = codeInput.trim();
    if (codeInput == code) {
        submitCodeDone(email, password, name, phone, true);
    } else {
        submitCodeDone(email, password, name, phone, false);
    }
};

/* return true when email exist */
async function checkEmailExist(email, password, name, phone) {
    mydata = {
        email: email,
        name: name,
        password: password,
        phone: phone,
    };

    const flag = await axios({
        method: "post",
        url: "http://hit.kmin.edu.vn/account/api/check_email_exist",
        data: mydata,
    })
        .then((res) => {
            return false;
        })
        .catch((err) => {
            $(".preloader-wrapper").remove();
            $("#submit-form").html("Đăng ký");
            let toastHTML = `<i class="material-icons">
          sentiment_very_dissatisfied
          </i><span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="red-text darken-3">${
              err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu"
          }</span></span>`;
            M.toast({ html: toastHTML });
            return true;
        });

    return flag;
}
