var load = `<span class="spinner-border text-warning spinner-border-sm" role="status" aria-hidden="true"></span>
Loading...`;

function Challenges(id) {
    var header = getCookie("Authorization");
    axios({
        headers: {
            Authorization: header,
        },
        method: "get",
        url: "http://hit.kmin.edu.vn/challenge/api/get_challenge_by_topic?id_topic=" + id,
    })
        .then((res) => {
            var data = res.data.data.challenges;
            var render = [];
            for (var val of data) {
                var detail = {
                    id: val.ID,
                    text: val.name_challenge,
                };
                render.push(detail);
            }
            $("#challenges").each(function () {
                $(this).select2({
                    placeholder: "Challenges",
                    data: render,
                });
            });
        })
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
}

(function ($) {
    "use strict";

    $("#challenges").each(function () {
        $(this).select2({
            placeholder: "Chọn một chủ để!",
        });
    });
})(jQuery);

(function ($) {
    "use strict";

    var header = getCookie("Authorization");
    axios({
        headers: {
            Authorization: header,
        },
        method: "get",
        url: "http://hit.kmin.edu.vn/topics/api/get_all_topics",
    })
        .then((res) => {
            var topics = [];
            var data = res.data.data;
            for (var val of data) {
                var detail_topic = {
                    id: val.ID,
                    text: val.name_topic,
                };
                topics.push(detail_topic);
            }
            $("#topics").each(function () {
                $(this).select2({
                    placeholder: "Chủ để",
                    data: topics,
                });
            });
        })
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
})(jQuery);

(function ($) {
    // USE STRICT
    "use strict";
    $(".animsition").animsition({
        inClass: "fade-in",
        outClass: "fade-out",
        inDuration: 900,
        outDuration: 900,
        linkElement: 'a:not([target="_blank"]):not([href^="#"]):not([class^="chosen-single"])',
        loading: true,
        loadingParentElement: "html",
        loadingClass: "page-loader",
        loadingInner: '<div class="page-loader__spin"></div>',
        timeout: false,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: ["animation-duration", "-webkit-animation-duration"],
        overlay: false,
        overlayClass: "animsition-overlay-slide",
        overlayParentElement: "html",
        transition: function (url) {
            window.location.href = url;
        },
    });
})(jQuery);

(function ($) {
    "use strict";

    var header = getCookie("Authorization");
    axios({
        headers: {
            Authorization: header,
        },
        method: "get",
        url: "http://hit.kmin.edu.vn/problem/api/get_all_problems",
    })
        .then((res) => {
            var data = res.data.data.problems;
            for (var val of data) {
                var Level;
                switch (val.level) {
                    case 1:
                        Level = "Dễ";
                        break;
                    case 2:
                        Level = "Bình thường";
                        break;
                    case 3:
                        Level = "Khó";
                        break;
                    case 4:
                        Level = "Cực khó";
                        break;
                }
                var DOM = `
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox" name="select-problem" class="select-problem" value="${val.ID}" />
                                <span class="au-checkmark premium"></span>
                            </label>
                        </td>
                        <td>
                            <a href="/admin_edit_problem?id=${
                                val.ID
                            }" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa problem này">
                                <i class="fas fa-edit"></i>
                            </a>
                        </td>
                        <td>${val.name_problem}</td>
                        <td>${val.premium == 0 ? "false" : "true"}</td>
                        <td>${Level}</td>
                        <td>
                            ${val.problem_need_to_be_solve}
                        </td>
                        <td>
                            ${val.description}
                        </td>
                        <td>${val.constraint ? val.constraint : "Không cần ràng buộc"}</td>
                        <td>${val.input_format ? val.input_format : "Không cần đầu vào"}</td>
                        <td>${val.output_format ? val.output_format : "Không cần đầu ra"}</td>
                    </tr>`;
                $("#render-problems").append(DOM);
            }
        })
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
        });
})(jQuery);

function RenderFilter(idChallenge) {
    var header = getCookie("Authorization");
    axios({
        headers: {
            Authorization: header,
        },
        method: "get",
        url: "http://hit.kmin.edu.vn/problem/api/get_problems_by_challenge?id_challenge=" + idChallenge,
    })
        .then((res) => {
            var data = res.data.data.problems;
            $("#render-problems").html("");
            for (var val of data) {
                var Level;
                switch (val.level) {
                    case 1:
                        Level = "Dễ";
                        break;
                    case 2:
                        Level = "Bình thường";
                        break;
                    case 3:
                        Level = "Khó";
                        break;
                    case 4:
                        Level = "Cực khó";
                        break;
                }
                var DOM = `
                    <tr>
                        <td>
                            <label class="au-checkbox">
                                <input type="checkbox" name="select-problem" class="select-problem" value="${val.ID}" />
                                <span class="au-checkmark premium"></span>
                            </label>
                        </td>
                        <td>
                            <a href="/admin_edit_problem?id=${
                                val.ID
                            }" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa problem này">
                                <i class="fas fa-edit"></i>
                            </a>
                        </td>
                        <td>${val.name_problem}</td>
                        <td>${val.premium == 0 ? "false" : "true"}</td>
                        <td>${Level}</td>
                        <td>
                            ${val.problem_need_to_be_solve}
                        </td>
                        <td>
                            ${val.description}
                        </td>
                        <td>${val.constraint ? val.constraint : "Không cần ràng buộc"}</td>
                        <td>${val.input_format ? val.input_format : "Không cần đầu vào"}</td>
                        <td>${val.output_format ? val.output_format : "Không cần đầu ra"}</td>
                    </tr>`;
                $("#render-problems").append(DOM);
            }
            $("#filter").html('<i class="zmdi zmdi-filter-list"></i>filters');
        })
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
            $("#noti-content").html(err);
            $("#notification").toast("show");
            $("#filter").html('<i class="zmdi zmdi-filter-list"></i>filters');
        });
}

function DeleteProblem(listID) {
    const header = getCookie("Authorization");
    axios({
        method: "delete",
        headers: {
            Authorization: header,
        },
        url: "http://hit.kmin.edu.vn/problem/api/delete",
        data: {
            list_id: listID,
        },
    })
        .then(() => {
            location.reload();
        })
        .catch((err) => {
            $("#noti-content").html(err.response.data ? err.response.data.error.message : "Không xử lý được yêu cầu");
            $("#notification").toast("show");
            $("#noti-content").html(err);
            $("#notification").toast("show");
        });
}

$(document).ready(function () {
    $("#topics").on("select2:select", function (e) {
        var data = e.params.data.id;
        Challenges(data);
    });

    $("#filter").on("click", function () {
        $("#filter").html("").append(load);
        var data = $("#challenges").val();
        RenderFilter(data);
    });

    $("#reset").on("click", function () {
        location.reload();
    });

    $("#delete-problem").on("click", () => {
        var dataDelete = [];
        $.each($("input[name='select-problem']:checked"), function () {
            dataDelete.push(parseInt($(this).val()));
        });
        if (dataDelete.length > 0) {
            if (confirm("Bạn có muốn xóa các Problem này?")) {
                DeleteProblem(dataDelete);
            }
        }
    });
});
