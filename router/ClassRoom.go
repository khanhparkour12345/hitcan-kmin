package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"math/rand"
	"net/http"

	"github.com/gin-gonic/gin"
)

type DataResponseClassRoom struct {
	User          models.Account
	Course        models.Course
	Chapter       models.Chapter
	Lesson        models.Lesson
	SearchURLNext string
	SearchURLPrev string
}

func CourseRoom(c *gin.Context) {

	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		c.Redirect(http.StatusSeeOther, "http://hit.kmin.edu.vn/login")
		return
	}

	if len(account.Courses) == 0 {
		c.Redirect(http.StatusSeeOther, "http://hit.kmin.edu.vn/course")
		return
	}

	courseName := c.Param("course")
	chapterName := c.Param("chapter")
	lessonName := c.Param("lesson")
	courseNow := models.Course{}
	chapterNow := models.Chapter{}
	lessonNow := models.Lesson{}
	var urlNext string
	var urlPrev string
	flag := false

	courseNow.SearchURL = courseName
	courseNow.Name = courseName
	courseNow.GetDetailCourseBySearchURLAnName(false)

	for i, chapter := range courseNow.Chapters {
		lenChapter := len(courseNow.Chapters)
		lenLesson := len(chapter.Lessons)
		chapter.Lessons = SortLesson(chapter.Lessons)
		if chapter.SearchURL == chapterName {
			chapterNow = chapter
			for j, lesson := range chapter.Lessons {
				if lessonName == lesson.SearchURL {
					lessonNow = lesson
					lessonNow.Videos = models.GetAllVideosByLessonID(lesson.ID)
					lessonNow.Docs = models.GetAllDocByLessonID(lesson.ID)
					flag = true
					if j > 0 {
						urlPrev = chapter.Lessons[j-1].SearchURL
					} else {
						if i > 0 {
							lenLessonPrev := len(courseNow.Chapters[i-1].Lessons)
							if lenLessonPrev > 0 {
								urlPrev = "/course/" + courseNow.SearchURL + "/" + courseNow.Chapters[i-1].SearchURL + "/" + courseNow.Chapters[i-1].Lessons[lenLessonPrev-1].SearchURL
							}
						}
					}
					if j < lenLesson-1 {
						urlNext = chapter.Lessons[j+1].SearchURL
					} else {
						if i < lenChapter-1 && len(courseNow.Chapters[i+1].Lessons) > 0 {
							urlNext = "/course/" + courseNow.SearchURL + "/" + courseNow.Chapters[i+1].SearchURL + "/" + courseNow.Chapters[i+1].Lessons[0].SearchURL
						}
					}
					break
				}
			}
		}
	}

	if flag == false && len(courseNow.Chapters) > 0 {
		chapterNow = courseNow.Chapters[0]
		if len(courseNow.Chapters[0].Lessons) > 1 {
			lessonNow = courseNow.Chapters[0].Lessons[0]
			lessonNow.Videos = models.GetAllVideosByLessonID(lessonNow.ID)
			lessonNow.Docs = models.GetAllDocByLessonID(lessonNow.ID)
			urlNext = "/course/" + courseNow.SearchURL + "/" + courseNow.Chapters[0].SearchURL + "/" + courseNow.Chapters[0].Lessons[1].SearchURL
		}
	} else if flag == false {
		courseNow.ID = 0
		chapterNow.ID = 0
		lessonNow.ID = 0
	}

	datares := DataResponseClassRoom{
		User:          account,
		Course:        courseNow,
		Chapter:       chapterNow,
		Lesson:        lessonNow,
		SearchURLNext: urlNext,
		SearchURLPrev: urlPrev,
	}

	c.HTML(http.StatusOK, "classroom.html", datares)
}

func SortLesson(lessons []models.Lesson) []models.Lesson {
	if len(lessons) < 2 {
		return lessons
	}

	left, right := 0, len(lessons)-1
	pivot := rand.Int() % len(lessons)
	lessons[pivot], lessons[right] = lessons[right], lessons[pivot]
	for i, _ := range lessons {
		if lessons[i].Stt < lessons[right].Stt {
			lessons[left], lessons[i] = lessons[i], lessons[left]
			left++
		}
	}

	lessons[left], lessons[right] = lessons[right], lessons[left]

	SortLesson(lessons[:left])
	SortLesson(lessons[left+1:])

	return lessons
}
