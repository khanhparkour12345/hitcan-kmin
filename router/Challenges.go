package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type dataResponse struct {
	User  models.Account
	Topic models.Topic
}

func GetChallenges(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		account.ID = 0
	}

	topicID, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		fmt.Println(err)
		PageNotFound(c)
		return
	}
	topic := models.Topic{}
	topic.ID = uint(topicID)
	err = topic.GetTopic()
	if err != nil {
		fmt.Println(err)
		PageNotFound(c)
		return
	}

	datares := dataResponse{
		User:  account,
		Topic: topic,
	}

	c.HTML(http.StatusOK, "challenge.html", datares)
}
