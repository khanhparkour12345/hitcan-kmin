package router

import (
	"hitcan-kmin/handlers"
	"hitcan-kmin/handlers/hsocket"
	"hitcan-kmin/models"
	"strconv"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type DataResponse struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data,omitempty"`
	Error   error       `json:"error,omitempty"`
	Message string      `json:"messenge,omitempty"`
}

func MigrateDB() {
	models.MigrateAll()
}

func HandlerTemplate() {
	router := gin.Default()

	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:8083"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "GET", "DELETE"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return origin == "https://github.com"
		},
		MaxAge: 12 * time.Hour,
	}))

	router.LoadHTMLGlob("template/*.html")
	router.Static("/css", "./template/css")
	router.Static("/js", "./template/js")
	router.Static("/img", "/var/www/hitcan-kmin/img")
	router.Static("/avatar", "/var/www/hitcan-kmin/avatar")
	router.Static("/file", "/var/www/hitcan-kmin/file")
	router.Static("/node_modules/cropperjs/dist", "./template/node_modules/cropperjs/dist")
	router.Static("/fonts/poppins", "./template/fonts/poppins")
	router.StaticFile("/favicon.png", "./var/www/hitcan-kmin/img/favicon.png")

	//page
	router.GET("/favicon.ico", faviconHandler)
	router.GET("/", Index)
	router.GET("/topic", GetChallenges)
	router.GET("/challenge", ProblemPage)
	router.GET("/challenge_group", ChallengeOfGroup)
	router.GET("/detail_problem", DetailProblem)
	router.GET("/profile", UserProfile)
	router.GET("/about", AboutUs)
	router.GET("/signup", SignupPage)
	router.GET("/login", LoginPage)
	router.GET("/search_result", Search)
	router.GET("/course/:course/:chapter/:lesson", CourseRoom)
	router.GET("/course/:course", CourseRoom)
	router.GET("/course", GetCourse)
	router.GET("/page_not_found", PageNotFound)

	//page admin
	router.GET("/admin_index", AdminIndex)
	router.GET("/admin_login", AdminLogin)
	router.GET("/admin_form_data", AdminForm)
	router.GET("/admin_table_data", AdminTable)
	router.GET("/admin_edit_problem", AdminEditProblem)
	router.GET("/admin_add_video", AdminAddVideo)
	router.GET("/admin_add_user", AdminAddUser)

	//API
	router.GET("/topics/api/get_all_topics", handlers.GetAllTopic)
	router.GET("/challenges/api/get_topic/:id/:loadtime", handlers.GetTopic)
	router.GET("/challenge/api/get_challenge/:id/:loadtime", handlers.GetChallenge)
	router.GET("/challenge/api/get_all_challenges", handlers.GetAllChallenge)
	router.GET("/challenge/api/get_challenge_by_topic", handlers.GetChallengeByTopicID)
	router.GET("/challenge/api/get_challenge/:id", handlers.GetChallengeByID)
	router.GET("/problem/api/get_problem/:id", handlers.GetProblem)
	router.GET("/problem/api/get_number_of_porblems", handlers.GetNumberOfProblems)
	router.GET("/problem/api/get_all_problems", handlers.GetAllProblems)
	router.GET("/problem/api/get_problems_by_challenge", handlers.GetProblemByChallengeID)
	router.GET("/auth/google/login", handlers.OauthGoogleLogin)
	router.GET("/google_login/call_back", handlers.GoogleCallBack)
	router.GET("/facebook_login/call_back", handlers.FacebookLogin)
	router.GET("/account/api/get_all_account", handlers.GetAllAccount)
	router.GET("/account/api/get_number_of_accounts", handlers.GetNumberOfAccounts)
	router.GET("/account/api/get_account_limit/:loadtime", handlers.GetAccountLimit)
	router.GET("/group/api/get_all_groups", handlers.GetAllGroup)
	router.GET("/group/api/get_account_of_group/:id", handlers.GetAccountOfGroup)
	router.GET("/group/api/get_challenge/:id/:loadtime", handlers.GetChallengeOfGroup)
	//router.GET("/auth/facebook/login", handlers.OauthFacebookLogin)
	//router.GET("/facebook_login/call_back", handlers.FacebookCallBack)
	router.GET("/feedback/api/get_number_of_feedbacks", handlers.GetNumberOfFeedback)
	router.GET("/feedback/api/get_detail_chart", handlers.GetFeedBackDetailChart)
	router.GET("/feedback/api/get_all_feedback/:loadtime", handlers.GetFeedBackLimit)
	router.GET("/feedback/api/get_number_notseen", handlers.GetNumberOfFeedbackNotSeen)
	router.GET("/chapter/api/get_chapters_by_couse/:id", handlers.GetChapterByCourseID)
	router.GET("/lesson/api/get_lessons_by_chapter/:id", handlers.GetLessonByChapterID)

	router.POST("/account/api/create_account", handlers.CreateAccount)
	router.POST("/account/api/send_email", handlers.SendEmail)
	router.POST("/account/api/check_email_exist", handlers.CheckEmailExist)
	router.POST("/account/api/info_login", handlers.CheckStatusLogin)
	router.POST("/account/api/logout", handlers.Logout)
	router.POST("/account/api/update_password", handlers.UpdateForgotPassword)
	router.POST("/account/api/add_user", handlers.AdminCreateUser)
	router.POST("/group/api/create_group", handlers.CreateGroup)
	router.POST("/api/form_feedback/:id_problem", handlers.CreateNewFeedBack)
	router.POST("/problem/api/create_problem", handlers.CreateProblem)
	router.POST("/problem/api/upload_image", handlers.UploadImage)
	router.POST("/feedback/api/get_detail_feedback", handlers.GetDetailFeedback)
	router.POST("/lesson/api/add_video", handlers.UploadVideosLesson)

	router.PUT("/account/api/update_password", handlers.UpdatePassword)
	router.PUT("/account/api/update_profile", handlers.UpdateProfile)
	router.PUT("/account/api/update_avatar", handlers.UpdateAvatar)
	router.PUT("/account/api/delete_avatar", handlers.DeleteteAvatar)
	router.PUT("/account/api/update_premium", handlers.UpdatePremium)
	router.PUT("/group/api/update_group", handlers.UpdateGroup)
	router.PUT("/challenge/api/update_challenge", handlers.UpdateChallenge)
	router.PUT("/problem/api/udpate_problem", handlers.UpdateProblem)

	router.DELETE("/account/api/delete_account", handlers.DeleteAccount)
	router.DELETE("/group/api/delete_group", handlers.DeleteGroup)
	router.DELETE("/problem/api/delete", handlers.DeleteProblem)

	// websocket
	hsocket.HHub = hsocket.NewHub()
	go hsocket.HHub.Run()
	router.GET("/ws/noti/feedback", hsocket.HandleSocket)
	router.Run(":8083")
}

func faviconHandler(c *gin.Context) {
	c.File("/var/www/hitcan-kmin/img/favicon.png")
}

func Pagination(c *gin.Context, limit int) (int, int) {
	var numOfPage int
	numOfPageStr := c.Query("page")

	if numOfPageStr == "" {
		return 1, 0
	} else {
		var err error
		numOfPage, err = strconv.Atoi(numOfPageStr)
		if err != nil {
			panic(err)
		}
	}

	if numOfPage <= 0 {
		return 1, 0
	}

	begin := (limit * numOfPage) - limit
	return numOfPage, begin
}

func GetPageToShow(numOfPages, pageIndex int) []int {
	var arrPage []int

	if numOfPages <= 5 {
		for i := 1; i <= numOfPages; i++ {
			arrPage = append(arrPage, i)
		}
		return arrPage
	}

	begin := pageIndex - 2
	end := pageIndex + 2

	if begin <= 2 {
		for i := 1; i <= 5; i++ {
			arrPage = append(arrPage, i)
		}
		return arrPage
	}

	if end > numOfPages {
		if end-numOfPages == 2 {
			begin -= 2
		} else {
			begin--
		}
		end = numOfPages
	}

	for i := begin; i <= end; i++ {
		arrPage = append(arrPage, i)
	}
	return arrPage
}
