package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type dataResponseDetail struct {
	User          models.Account
	Problem       models.Problem
	NextProblem   uint   `json:"id_next_problem,omitempty"`
	NameTopic     string `json:"name_topic,omitempty"`
	IDTopic       uint   `json:"id_topic,omitempty"`
	NameChallenge string `json:"name_challenge,omitempty"`
	IDChallenge   uint   `json:"id_challenge,omitempty"`
}

func DetailProblem(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		account.ID = 0
		account.Premium = 0
	}

	problemID, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		PageNotFound(c)
		return
	}

	nameTopic := c.Query("name_topic")
	if nameTopic == "" {
		problem := models.Problem{}
		problem.ID = uint(problemID)
		err = problem.GetProblem()
		if err != nil {
			PageNotFound(c)
			return
		}

		if account.Premium == 0 && problem.Premium != 0 {
			PageNotFound(c)
			return
		}

		dataRes := dataResponseDetail{
			User:    account,
			Problem: problem,
		}
		c.HTML(http.StatusOK, "detail.html", dataRes)
		return
	}

	nameChallenge := c.Query("name_challenge")
	idTopic, err := strconv.Atoi(c.Query("id_topic"))
	if err != nil {
		fmt.Println(err)
		PageNotFound(c)
		return
	}
	idChallenge, err := strconv.Atoi(c.Query("id_challenge"))
	if err != nil {
		fmt.Println(err)
		PageNotFound(c)
		return
	}

	problem := models.Problem{}
	problem.ID = uint(problemID)
	err = problem.GetProblem()
	if err != nil {
		fmt.Println(err)
		PageNotFound(c)
		return
	}

	idNext := problem.GetNextProblem(account.Premium)
	if idNext != 0 {
		if account.Premium == 0 && problem.Premium != 0 {
			PageNotFound(c)
			return
		}

		dataRes := dataResponseDetail{
			User:          account,
			Problem:       problem,
			NextProblem:   idNext,
			NameTopic:     nameTopic,
			NameChallenge: nameChallenge,
			IDTopic:       uint(idTopic),
			IDChallenge:   uint(idChallenge),
		}

		c.HTML(http.StatusOK, "detail.html", dataRes)
		return
	}

	if account.Premium == 0 && problem.Premium != 0 {
		PageNotFound(c)
		return
	}

	dataRes := dataResponseDetail{
		User:          account,
		Problem:       problem,
		NameTopic:     nameTopic,
		NameChallenge: nameChallenge,
		IDTopic:       uint(idTopic),
		IDChallenge:   uint(idChallenge),
	}

	c.HTML(http.StatusOK, "detail.html", dataRes)
}
