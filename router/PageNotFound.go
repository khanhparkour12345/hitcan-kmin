package router

import (
	"net/http"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"

	"github.com/gin-gonic/gin"
)

type dataPageNotFound struct {
	User      models.Account
}

func PageNotFound(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		account.ID = 0
	}

	datares := dataPageNotFound{
		User:       account,
	}

	c.HTML(http.StatusNotFound, "page_not_found.html", datares)
}