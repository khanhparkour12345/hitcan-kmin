package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func SignupPage(c *gin.Context) {

	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	if email != nil || id != nil {
		PageNotFound(c)
		return
	}

	c.HTML(http.StatusOK, "signup.html", nil)
}
