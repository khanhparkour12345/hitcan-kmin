package router

import (
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type adminIndexData struct {
	Account    models.Account
	ListGroups models.ListGroups
	ListCourse []models.Course
	Chapters   []models.Chapter
	Lessons    []models.Lesson
}

func AdminIndex(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		PageNotFound(c)
		return
	}

	if account.Premium < 2 {
		PageNotFound(c)
		return
	}

	groups := models.ListGroups{}
	if err := groups.GetAllGroup(); err != nil {
		PageNotFound(c)
		return
	}

	data := adminIndexData{
		Account:    account,
		ListGroups: groups,
	}

	c.HTML(http.StatusOK, "admin.html", data)
}

func AdminLogin(c *gin.Context) {
	c.HTML(http.StatusOK, "admin_login.html", nil)
}

func AdminForm(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		PageNotFound(c)
		return
	}

	if account.Premium < 2 {
		PageNotFound(c)
		return
	}

	data := adminIndexData{
		Account: account,
	}

	c.HTML(http.StatusOK, "admin_form.html", data)
}

func AdminTable(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		PageNotFound(c)
		return
	}

	if account.Premium < 2 {
		PageNotFound(c)
		return
	}

	data := adminIndexData{
		Account: account,
	}

	c.HTML(http.StatusOK, "admin_table.html", data)
}

func AdminEditProblem(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		PageNotFound(c)
		return
	}

	if account.Premium < 2 {
		PageNotFound(c)
		return
	}

	idProblem, err := strconv.Atoi(c.Query("id"))
	if err != nil {
		panic(err)
	}
	problem := models.Problem{}
	problem.ID = uint(idProblem)
	if err := problem.GetProblem(); err != nil {
		panic(err)
	}

	if err := problem.GetChallengesOfProblem(); err != nil {
		panic(err)
	}

	type dataResponse struct {
		Problem models.Problem
		Account models.Account
	}

	data := dataResponse{
		Problem: problem,
		Account: account,
	}

	c.HTML(http.StatusOK, "admin_edit_problem.html", data)
}

func AdminAddVideo(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		PageNotFound(c)
		return
	}

	if account.Premium < 2 {
		PageNotFound(c)
		return
	}

	courses, chapters, lessons := handlers.ComsumDataForAdminAddVideo()

	lessons = SortLesson(lessons)

	data := adminIndexData{
		Account:    account,
		ListCourse: courses,
		Chapters:   chapters,
		Lessons:    lessons,
	}

	c.HTML(http.StatusOK, "admin_add_video.html", data)
}

func AdminAddUser(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			panic(err)
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			panic(err)
		}
	} else {
		PageNotFound(c)
		return
	}

	if account.Premium < 2 {
		PageNotFound(c)
		return
	}

	courses := handlers.ConsumDataForAdminAddUser()

	data := adminIndexData{
		Account:    account,
		ListCourse: courses,
	}

	c.HTML(http.StatusOK, "admin_add_user.html", data)
}
