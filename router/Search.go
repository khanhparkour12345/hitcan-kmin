package router

import (
	"fmt"
	"hitcan-kmin/handlers"
	"hitcan-kmin/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type dataResponseSearch struct {
	User models.Account
	Data models.ResultOfSearch
}

func Search(c *gin.Context) {
	tokenString, _ := c.Cookie("Authorization")

	var email, id interface{}
	if tokenString != "" {
		var err error
		email, id, err = handlers.ParseTokenString(tokenString)
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	}

	account := models.Account{}
	if id != nil || email != nil {
		idInt := id.(float64)
		idAccount := uint(idInt)
		account.ID = idAccount
		account.Email = email.(string)
		err := account.SelectAccount()
		if err != nil {
			fmt.Println(err)
			PageNotFound(c)
			return
		}
	} else {
		account.ID = 0
	}

	keyword := c.Query("keyword")

	dataResult, err := models.SearchForKeyword(keyword, account.Premium)
	if err != nil {
		fmt.Println(err)
		PageNotFound(c)
		return
	}

	data := dataResponseSearch{
		User: account,
		Data: dataResult,
	}

	c.HTML(http.StatusOK, "search_result.html", data)
}
